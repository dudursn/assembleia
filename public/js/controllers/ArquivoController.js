class ArquivoController{

    constructor(){

        
    }

    addUploadFileComponent(){
        let self = this
        if ($('.multiple-file-upload').length > 0) {
            var itemTemplate = $('#mfu-item-template');
            var itemsContainer = $('#mfu-items-container');
            var itemNumber = 1;
    
            self._createMfuItem(itemTemplate, itemsContainer, itemNumber);
            itemNumber++;
    
            $('#mfu-new-item-btn').click(function(e) {
                self._createMfuItem(itemTemplate, itemsContainer, itemNumber);
                itemNumber++;
            });
        }
    }

    removeFile(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "DELETE",
            url: $(e.target).attr('data-url'),
            type: 'delete', // replaced from put
            dataType: "JSON",
            data: {
                "id": $(e.target).attr('data-send') // method and token not needed in data
            },
            success:function(response){
              
                $(e.target).closest('.forDelete').find('.server-response').html(response.msg).show();
                let activitySelectWrap = $(e.target).closest(".forDelete");
                activitySelectWrap.remove();
            },
            error:function(xhr){
                
                console.log(xhr.responseText);
                $(e.target).closest('.forDelete').find('.server-response').html('Erro ao remover o arquivo')
                .show();
            }
        })
      
    }

    _createMfuItem(itemTemplate, itemsContainer, itemNumber) {
        let itemTemplateHtml = itemTemplate.html().replace(/\{ITEM_NUMBER\}/g, itemNumber);
        itemsContainer.append(itemTemplateHtml);
    }
    
}