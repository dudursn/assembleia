class CondominoController{

    constructor(){

        this._divCpf = document.querySelector("#divCpf");
        this._divCnpj = document.querySelector("#divCnpj");
        this._selTipoPessoa = document.querySelector('#selTipoPessoa');
    }


    onchangeSelTipoPessoa(){
        let opt = this._selTipoPessoa.options[this._selTipoPessoa.selectedIndex].value;
        if(opt==1){

            this._divCpf.style.display = '';
            this._divCnpj.style.display = 'none';

            document.querySelector("#txtCpf").value = '';
        }else if(opt==2){

            this._divCpf.style.display = 'none';
            this._divCnpj.style.display = '';
            document.querySelector("#txtCnpj").value = '';
        }
    }

    
}