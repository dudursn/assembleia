class AssembleiaController{

    constructor(){

        this._arquivoController = new ArquivoController();
    }

    init(){

        this._arquivoController.addUploadFileComponent();
    }

    removeFile(e){
        this._arquivoController.removeFile(e);      
    }

    
}