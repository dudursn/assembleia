class DataTableCompontent{

    constructor(dataTableId){

        this._dataTableId = dataTableId;
    }

    init(){

        let self = this;
        if(this._dataTableId){
            $(document).ready(function() {
                
                $(self._dataTableId).DataTable({
                    initComplete: function () {
                        this.api().columns().every( function () {
                            var column = this;
                            var select = $('<select class="custom-select"><option value=""></option></select>')
                                .appendTo( $(column.footer()).empty() )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
             
                                    column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                } );
             
                            column.data().unique().sort().each( function ( d, j ) {
                                select.append( '<option value="'+d+'">'+d+'</option>' )
                            } );
                        } );
                    },
            
                    "language":{
                        "sProcessing":   "A processar...",
                        "sLengthMenu":   "Mostrar _MENU_ registos",
                        "sZeroRecords":  "Não foram encontrados resultados",
                        "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                        "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
                        "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                        "sInfoPostFix":  "",
                        "sSearch":       "Procurar:",
                        "sUrl":          "",
                        "oPaginate": {
                            "sFirst":    "Primeiro",
                            "sPrevious": "<i class=\"material-icons\">skip_previous</i>",
                            "sNext":     "<i class=\"material-icons\">skip_next</i>",
                            "sLast":     "Último"
                        }

                   
                    }
                });
            } );
        }
    }
}