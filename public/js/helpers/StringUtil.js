class StringUtil {
    static capitalizeAndSeparate(texto) {

        let t = '';
        t = texto.charAt(0).toUpperCase();

        for (let i = 1; i < texto.length; i++) {
            if (texto.charAt(i) == texto.charAt(i).toUpperCase()) {
                t += ' ' + texto.charAt(i);
            }
            else {
                t += texto.charAt(i);
            }
        }

        return t;
    }

    static separateWords(texto, separador) {

        let t = '';

        for (let i = 0; i < texto.length; i++) {
            if ((i > 0) && (texto.charAt(i) == texto.charAt(i).toUpperCase())) {
                t += separador + texto.charAt(i);
            }
            else {
                t += texto.charAt(i);
            }
        }

        return t.toLowerCase();
    }

    static initialToLowerCase(texto) {

        let t = texto.substr(0, 1).toLowerCase();
        t += texto.substr(1);

        return t;
    }


    static mask(valor, mascara) {
        let valorFormatado = '';
        let k = 0;
        for (let i = 0; i < mascara.length; i++) {
            if (mascara[i] == '#') {
                valorFormatado += valor[k];
                k++;
            }
            else {
                valorFormatado += mascara[i];
            }
        }
        return valorFormatado;
    }     

    static limparMascara(valor, caracteres){
        if(valor!=''){
            let re = new RegExp('([' + caracteres + '])',"g");
            return valor.replace(re,'');
        }
        return '';
    }
}