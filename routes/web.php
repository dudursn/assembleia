<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!

Route::get("/", function () {
    return view("welcome");
});
|
*/



//Testes para fins didáticos
/*
Route::get("/requisicoes", "SeriesController@testarRequests");

//Aulas
Route::get("/", "SeriesController@index")->name("listar_series");
Route::get("/series", "SeriesController@index")->name("listar_series");

Route::get("/series/criar", "SeriesController@create")
    ->name("form_criar_serie")
    ->middleware('auth');
    
Route::post("/series/criar", "SeriesController@store")->middleware('auth');
Route::post("/series/editar/{id}", "SeriesController@editarSerie")->middleware('auth');
Route::delete("/series/remover/{serie_id}", "SeriesController@destroyWithoutDeleteOnCascade")
    ->name("remover_serie")
    ->middleware('auth');

Route::get("/series/{serie_id}/temporadas", "TemporadasController@index")
    ->name("listar_temporadas");

Route::get("/temporadas/{temporada}/episodios", "EpisodiosController@index")
    ->name("listar_episodios");

Route::post("/temporadas/{temporada}/episodios/assistir", "EpisodiosController@assistir")
    ->name("form_episodios_assistidos")
    ->middleware('auth');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/entrar', 'EntrarController@index')->name('home');
Route::post('/entrar', 'EntrarController@entrar')->name('entrar');
Route::get('/registrar', 'RegistrarController@create')->name('form_registrar');
Route::post('/registrar', 'RegistrarController@store')->name('registrar');

Route::get('/sair', function () {

    Auth::logout();
    return redirect('entrar');
})->name('sair');Auth::routes();
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get("/", "HomeController@index")->name('home')->middleware('auth');


Route::group(['middleware' => 'auth'], function () {
	
	Route::get('assembleia','AssembleiaController@index')->name('assembleia');

	//Administrador
	Route::group(['middleware' => 'admin:1'], function () {
		
		Route::get('typography', function () {
			return view('pages.typography');
		})->name('typography');

		Route::get('icons', function () {
			return view('pages.icons');
		})->name('icons');

		Route::get('map', function () {
			return view('pages.map');
		})->name('map');

		Route::get('notifications', function () {
			return view('pages.notifications');
		})->name('notifications');

		Route::get('rtl-support', function () {
			return view('pages.language');
		})->name('language');
		/**------------------ */


		
		Route::get('table-list', function () {
			return view('pages.table_list');
		})->name('table');

		Route::get('blank', function () {
			return view('pages.blank');
		})->name('blank');

		Route::get('form', function () {
			return view('pages.form');
		})->name('form');

				
		Route::group(['prefix' => 'tipoPessoa'], function () {
			Route::get('','TipoPessoaController@index')->name('tipo_pessoa');
			Route::get('criar','TipoPessoaController@create')->name('create_tipo_pessoa');
			Route::post('salvar','TipoPessoaController@store')->name('save_tipo_pessoa');

			Route::get('editar/{id}','TipoPessoaController@edit')->name('edit_tipo_pessoa');
			Route::put('salvar', "TipoPessoaController@update")->name('save_tipo_pessoa');
			Route::delete("{id}", "TipoPessoaController@destroy")->name('delete_tipo_pessoa');
		});

		Route::group(['prefix' => 'tipoMorador'], function () {
			Route::get('','TipoMoradorController@index')->name('tipo_morador');
			Route::get('criar','TipoMoradorController@create')->name('create_tipo_morador');
			Route::post('salvar','TipoMoradorController@store')->name('save_tipo_morador');
	
			Route::get('editar/{id}','TipoMoradorController@edit')->name('edit_tipo_morador');
			Route::put('salvar', "TipoMoradorController@update")->name('save_tipo_morador');
			Route::delete("{id}", "TipoMoradorController@destroy")->name('delete_tipo_morador');
		});

		Route::group(['prefix' => 'situacao_assembleia'], function () {
			Route::get('','SituacaoAssembleiaController@index')->name('situacao_assembleia');
			Route::get('criar','SituacaoAssembleiaController@create')->name('create_situacao_assembleia');
			Route::post('salvar','SituacaoAssembleiaController@store')->name('save_situacao_assembleia');
			Route::get('editar/{id}','SituacaoAssembleiaController@edit')->name('edit_situacao_assembleia');
			Route::put('salvar', "SituacaoAssembleiaController@update")->name('save_situacao_assembleia');
			Route::delete("{id}", "SituacaoAssembleiaController@destroy")->name('delete_situacao_assembleia');
		});
		

		Route::group(['prefix' => 'torre'], function () {
			Route::get('','TorreController@index')->name('torre');
			Route::get('criar','TorreController@create')->name('create_torre');
			Route::post('salvar','TorreController@store')->name('save_torre');
			Route::get('editar/{id}','TorreController@edit')->name('edit_torre');
			Route::put('salvar', "TorreController@update")->name('save_torre');
			Route::delete("{id}", "TorreController@destroy")->name('delete_torre');
		});

		Route::group(['prefix' => 'unidade'], function () {
			Route::get('','UnidadeController@index')->name('unidade');
			Route::get('criar','UnidadeController@create')->name('create_unidade');
			Route::post('salvar','UnidadeController@store')->name('save_unidade');
	
			Route::get('editar/{id}','UnidadeController@edit')->name('edit_unidade');
			Route::put('salvar', "UnidadeController@update")->name('save_unidade');
			Route::delete("{id}", "UnidadeController@destroy")->name('delete_unidade');
		});

		Route::group(['prefix' => 'condomino'], function () {
			Route::get('','CondominoController@index')->name('condomino');
			Route::get('criar','CondominoController@create')->name('create_condomino');
			Route::post('salvar','CondominoController@store')->name('save_condomino');
	
			Route::get('editar/{id}','CondominoController@edit')->name('edit_condomino');
			Route::put('salvar', "CondominoController@update")->name('save_condomino');
			Route::delete("{id}", "CondominoController@destroy")->name('delete_condomino');
		});

		Route::group(['prefix' => 'assembleia'], function () {
			
			Route::get('criar','AssembleiaController@create')->name('create_assembleia');
			Route::post('salvar','AssembleiaController@store')->name('save_assembleia');
	
			Route::get('editar/{id}','AssembleiaController@edit')->name('edit_assembleia');
			Route::put('salvar', "AssembleiaController@update")->name('save_assembleia');

			Route::get("{id}", "AssembleiaController@show");
			Route::delete("{id}", "AssembleiaController@destroy");
		});

		Route::group(['prefix' => 'usuario'], function () {
			Route::get('','UserController@index')->name('usuario');
			Route::get('criar','UserController@create')->name('create_usuario');
			Route::post('salvar','UserController@store')->name('save_usuario');
	
			Route::get('editar/{id}','UserController@edit')->name('edit_usuario');
			Route::put('salvar', "UserController@update")->name('save_usuario');
			Route::delete("{id}", "UserController@destroy")->name('delete_usuario');
		});

		Route::group(['prefix' => 'arquivos'], function () {
			
			Route::delete('deleteJson/{id}', 'ArquivoController@deleteJson')->name('delete_json_arquivo');
		});

	});

});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Auth::routes();
