@if ($errors->any())
    @component("components.alert")
        @slot("tipo", "alert-danger")
        @slot("title", "Erro")
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>

    @endcomponent
@endif

@if(isset($mensagem) && isset($tipoAlert))

    @component("components.alert")
        @slot("tipo", $tipoAlert)
        @slot("title")
        Mensagem
        @endslot
        {{ $mensagem }}
    @endcomponent

@endif