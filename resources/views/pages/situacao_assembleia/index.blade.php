<?php use App\Configs\SituacaoAssembleiaConfig; ?>

@extends('layouts.app', ['activePage' => 'situacao_assembleia', 'titlePage' => __('Situação de Assembleias')])

<link rel="stylesheet" type="text/css" href="{!! asset('js/terceiros/DataTables/css/jquery.dataTables.min.css') !!}">


@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="container-fluid">

        <div class="col-sm-10">
            <div class="form-group bmd-form-group">
                <a href="{{ route('create_situacao_assembleia') }}" role="button" class="btn btn-info" >
                    <span class="material-icons">add</span>
                    <span>{{ SituacaoAssembleiaConfig::titleNew() }}</span>
                </a>

            </div>
        </div>

        @include('components.messages_feedback')

      <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">{{ SituacaoAssembleiaConfig::titleCardPageIndex() }}</h4>
                        <p class="card-category">{{ SituacaoAssembleiaConfig::categoryCardPageIndex() }}</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="situacoesAssembleias" class="table display">
                                <thead class=" text-primary">
									@foreach ($tableThLabels as $label)
                                    <th>
                                        {{ $label }}
                                    </th>
								 
									@endforeach
                                    <th class="td-actions text-right">
                                        Ações
                                    </th>

                                </thead>
                                <tbody>

									@foreach ($situacoesAssembleias as $situacaoAssembleia)

                                    <tr>

                                        <td><a href="{{ route('edit_situacao_assembleia', $situacaoAssembleia->id) }}"> {{ $situacaoAssembleia->id}} </a></td>
                                        
                                        <td class="text-primary"><a href="{{ route('edit_situacao_assembleia', $situacaoAssembleia->id) }}">{{ $situacaoAssembleia->nome}} </a></td>

                                        <td class="text-right">
                                            <span>
                                                
                                                <span>
                                                    <a  role="button" href="{{ route('edit_situacao_assembleia', $situacaoAssembleia->id) }}" title="Editar" class="btn btn-primary btn-link btn-sm">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                </span>
                                                

                                                @if (Auth::user() && Auth::user()->admin)
                                                <span>
                                                    <form  method="post" action="{{ route('delete_situacao_assembleia', ['id' => $situacaoAssembleia->id]) }}"
                                                        onsubmit="return confirm('Tem certeza que deseja remover {{ addslashes( $situacaoAssembleia->nome )}}?')">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" title="Remover"  class="btn btn-danger btn-link btn-sm">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                    </form>
                                                </span>
                                                @endif
                                            
                                            </span>
                                        </td>
                                    </tr>
									@endforeach
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{!! asset('js/terceiros/DataTables/js/jquery-3.5.1.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/terceiros/DataTables/js/jquery.dataTables.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/components/DataTableCompontent.js') !!}"></script>
<script type="text/javascript">
	let dataTableComponent = new DataTableCompontent('#situacoesAssembleias');
	dataTableComponent.init();
</script>
@endsection