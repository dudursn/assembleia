@extends('layouts.app', ['activePage' => 'form', 'titlePage' => __('Form')])

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">
       

            <div class="col-md-12">
            
                <div class="card ">
                    <div class="card-header card-header-rose card-header-text">
                        <div class="card-text">
                            <h4 class="card-title">Form Elements</h4>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form method="get" action="/" class="form-horizontal">
                            <div class="row">
                                <label class="col-sm-2 col-form-label">With help</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control">
                                        <span class="bmd-help">A block of help text that breaks onto a new line.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Input Select</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <select class="custom-select" id="inlineFormCustomSelect">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">

                                <label for="txtHoraInicio" class="col-sm-2 col-form-label">Hora Início</label>
                                <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="time" value="13:45:00" id="txtHoraInicio">
                                
                                    </div>
                                </div>

                                <label  for="txtHoraFim" class="col-sm-2 col-form-label">Hora Fim</label>
                                <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="time" value="13:45:00" id="txtHoraInicio">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label for="txtDataInicio" class="col-sm-2 col-form-label">Data Início</label>
                                <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="date" value="2011-08-19" id="txtDataInicio">

                                    </div>
                                </div>

                                <label for="txtDataEncerramento" class="col-sm-2 col-form-label">Data Encerramento</label>
                                <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="date" value="2011-08-19" id="txtDataEncerramento">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="password" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Placeholder</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control" placeholder="placeholder">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Disabled</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group is-filled">
                                        <input type="text" class="form-control" value="Disabled input here.."
                                            disabled="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Static control</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <p class="form-control-static">hello@creative-tim.com</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label label-checkbox">Checkboxes and radios</label>
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" value=""> First Checkbox
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" value=""> Second Checkbox
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                value="option2" checked=""> First Radio
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                value="option1"> Second Radio
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label label-checkbox">Inline checkboxes</label>
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" value=""> a
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" value=""> b
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" value=""> c
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">File Upload</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">

                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"> 
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                <span class="fileinput-filename"></span>
                                            </div> 
                                            <span class="input-group-addon btn btn-default btn-file"> 
                                                <span class="fileinput-exists">Selecione</span>
                                                <input type="hidden">
                                                <input type="file" name="fileUpload"> 
                                            </span> 
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">
                                                Remover
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection