

@extends('layouts.app', ['activePage' => $data->activePage, 'titlePage' => __($data->titlePage)])

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">

            @include('components.messages_feedback')
          
            <div class="col-md-12">

                <div class="card ">
                    <div class="card-header card-header-rose card-header-text">
                        <div class="card-text">
                            <h4 class="card-title">{{ $data->cardTitle}}</h4>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('save_usuario') }}" class="form-horizontal"
                            enctype='multipart/form-data'>
                            @if(isset($data->id) && $data->id!=0)
                                <input type="hidden" id="txtId" name="txtId" value="{{ $data->id }}">
                                @method('PUT')
                            @endif

                            @csrf
                            
                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Nome</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control" id="txtNome" name="txtNome" readonly
                                            value="{{ old('txtNome') ? old('txtNome') : $data->name}}">
                                        <span class="bmd-help">Nome do Usuário - Somente Leitura.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Email</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="email" class="form-control " id="txtEmail" name="email" readonly
                                        maxlength="200" value="{{ old('txtEmail') ? old('txtEmail') : $data->email}}">
                                        <span class="bmd-help">Email - Somente Leitura.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label label-checkbox">Administrador</label>
                                <div class="col-sm-10 checkbox-radios">

                                    @php
                                    $checked = old('optAdmin') ? old('optAdmin') :
                                    $data->admin;
                                    @endphp
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="optAdmin"
                                                {{ $checked == '1' ? 'checked' : '' }} 
                                                value="1"> Sim
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="optAdmin"
                                                {{ $checked != '1' ? 'checked' : '' }} 
                                                value="0"> Não
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            @php /*
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Gerar senha aleatória</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <a role="button" onclick="userController.generateRandomStr(document.querySelector('#txtRandomPass'))"
                                        class="btn btn-info">{{ __('Gerar') }}</a>

                                        <input type="text" class="form-control " id="txtRandomPass" name="txtRandomPass" 
                                        maxlength="8" value="{{ old('txtRandomPass') }}" readonly>
                                       
                                    </div>
                                </div>
                            </div>
                            */
                            @endphp
                            <div class="card ">

                                <div class="card-footer ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                                    <a role="button" href="{{ route('usuario') }}" class="btn btn-light">{{ __('Voltar') }}</a>

                                </div>
                               
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{!! asset('js/controllers/UserController.js') !!}"></script>
<script type="text/javascript">
    let userController = new UserController();
</script>
@endsection