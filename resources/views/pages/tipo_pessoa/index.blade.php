<?php use App\Configs\TipoPessoaConfig; ?>

@extends('layouts.app', ['activePage' => 'tipo_pessoa', 'titlePage' => __('Tipo de Pessoas')])

<link rel="stylesheet" type="text/css" href="{!! asset('js/terceiros/DataTables/css/jquery.dataTables.min.css') !!}">


@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="container-fluid">

        <div class="col-sm-10">
            <div class="form-group bmd-form-group">
                <a href="{{ route('create_tipo_pessoa') }}" role="button" class="btn btn-info" >
                    <span class="material-icons">add</span>
                    <span>{{ TipoPessoaConfig::titleNew() }}</span>
                </a>

            </div>
        </div>

        @include('components.messages_feedback')

      <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">{{ TipoPessoaConfig::titleCardPageIndex() }}</h4>
                        <p class="card-category">{{ TipoPessoaConfig::categoryCardPageIndex() }}</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tipoPessoas" class="table display">
                                <thead class=" text-primary">
									@foreach ($tableThLabels as $label)
                                    <th>
                                        {{ $label }}
                                    </th>
								 
									@endforeach
                                    <th class="td-actions text-right">
                                        Ações
                                    </th>

                                </thead>
                                <tbody>

									@foreach ($tipoPessoas as $tipoPessoa)

                                    <tr>

                                        <td><a href="{{ route('edit_tipo_pessoa', $tipoPessoa->id) }}"> {{ $tipoPessoa->id}} </a></td>
                                        
                                        <td class="text-primary"><a href="{{ route('edit_tipo_pessoa', $tipoPessoa->id) }}">{{ $tipoPessoa->nome}} </a></td>

                                        <td class="text-right">
                                            <span>
                                                
                                                <span>
                                                    <a  role="button" href="{{ route('edit_tipo_pessoa', $tipoPessoa->id) }}" title="Editar" class="btn btn-primary btn-link btn-sm">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                </span>
                                                

                                                @if (Auth::user() && Auth::user()->admin)
                                                <span>
                                                    <form  method="post" action="{{ route('delete_tipo_pessoa', ['id' => $tipoPessoa->id]) }}"
                                                        onsubmit="return confirm('Tem certeza que deseja remover {{ addslashes( $tipoPessoa->nome )}}?')">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" title="Remover"  class="btn btn-danger btn-link btn-sm">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                    </form>
                                                </span>
                                                @endif
                                            
                                            </span>
                                        </td>
                                    </tr>
									@endforeach
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{!! asset('js/terceiros/DataTables/js/jquery-3.5.1.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/terceiros/DataTables/js/jquery.dataTables.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/components/DataTableCompontent.js') !!}"></script>
<script type="text/javascript">
	let dataTableComponent = new DataTableCompontent('#tipoPessoas');
	dataTableComponent.init();
</script>
@endsection