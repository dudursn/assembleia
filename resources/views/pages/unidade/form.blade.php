

@extends('layouts.app', ['activePage' => $data->activePage, 'titlePage' => __($data->titlePage)])

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">

            @include('components.messages_feedback')
          
            <div class="col-md-12">

                <div class="card ">
                    <div class="card-header card-header-rose card-header-text">
                        <div class="card-text">
                            <h4 class="card-title">{{ $data->cardTitle}}</h4>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('save_unidade') }}" class="form-horizontal"
                            enctype='multipart/form-data'>
                            @if(isset($data->id) && $data->id!=0)
                                <input type="hidden" id="txtId" name="txtId" value="{{ $data->id }}">
                                @method('PUT')
                            @endif

                            @csrf
                            
                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Nº da unidade</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="number" class="form-control" id="txtNumero" name="txtNumero" min="0"
                                            value="{{ old('txtNumero') ? old('txtNumero') : $data->numero}}">
                                        <span class="bmd-help">Número da Unidade. Deve ser maior que 0</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Fração Ideal</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input readonly type="text" class="form-control" id="txtFracao" name="txtFracao" 
                                            value="{{ old('txtFracao') ? old('txtFracao') : $data->fracao}}">
                                        <span class="bmd-help">Fração Ideal</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Torre</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <select class="custom-select" name="selTorre" id="selTorre">
                                            @php
                                            $selected = old('selTorre') ? old('selTorre') : $data->torre_id;
                                            @endphp

                                            <option disabled selected>Escolha...</option>
                                            @foreach($torres as $torre)
                                                <option {{ $selected == $torre->id ? ' selected' : '' }} value="{{$torre->id}}">{{$torre->nome}}
                                                </option>

                                            @endforeach
                                          

                                        </select>
                                    </div>
                                </div>
                            </div>
                           

                            <div class="card ">

                                <div class="card-footer ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                                    <a role="button" href="{{ route('unidade') }}" class="btn btn-light">{{ __('Voltar') }}</a>

                                </div>
                               
                            </div>





                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection