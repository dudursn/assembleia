<?php use App\Configs\CondominoConfig; ?>

@extends('layouts.app', ['activePage' => 'condomino', 'titlePage' => __('Condominos')])

<link rel="stylesheet" type="text/css" href="{!! asset('js/terceiros/DataTables/css/jquery.dataTables.min.css') !!}">

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">
            @if (Auth::user() && Auth::user()->admin)
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <a href="{{ route('create_condomino') }}" role="button" class="btn btn-info">
                        <span class="material-icons">add</span>
                        <span>{{ CondominoConfig::titleNew() }}</span>
                    </a>

                </div>
            </div>

            @include('components.messages_feedback')
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ CondominoConfig::titleCardPageIndex() }}</h4>
                            <p class="card-category">{{ CondominoConfig::categoryCardPageIndex() }}</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="condominos" class="table display">
                                    <thead class=" text-primary">
                                        @foreach ($tableThLabels as $label)
                                        <th>
                                            {{ $label }}
                                        </th>

                                        @endforeach
                                        <th class="text-right">
                                            Ações
                                        </th>

                                    </thead>
                                    <tbody>

                                        @foreach ($condominos as $condomino)

                                        <tr>
                                            <td><a href="{{ route('edit_condomino', $condomino->id) }}"> {{ $condomino->id}} </a>
                                            </td>

                                            <td class="text-primary"><a
                                                    href="{{ route('edit_condomino', $condomino->id) }}">{{ $condomino->nome}} </a>
                                            </td>

                                            <td class="text-primary"><a
                                                href="{{ route('edit_condomino', $condomino->id) }}">{{ $condomino->email}} </a>
                                            </td>

                                            <td class="text-primary"><a
                                                href="{{ route('edit_condomino', $condomino->id) }}">{{ $condomino->tipoPessoa->nome}} </a>
                                            </td>

                                            <td class="text-primary"><a
                                                href="{{ route('edit_condomino', $condomino->id) }}">{{ $condomino->tipoMorador->nome}} </a>
                                            </td>

                                            <td class="text-primary"><a
                                                href="{{ route('edit_condomino', $condomino->id) }}">{{ $condomino->unidade->full_name}} </a>
                                            </td>

                                            <td class="text-right">
                                                <span>
                                                    
                                                    <span>
                                                        <a role="button" href="{{ route('edit_condomino', $condomino->id) }}"
                                                            title="Editar" class="btn btn-primary btn-link btn-sm">
                                                            <i class="material-icons">edit</i>
                                                        </a>
                                                    </span>


                                                    @if (Auth::user() && Auth::user()->admin)
                                                    <span>
                                                        <form method="post"
                                                            action="{{ route('delete_condomino', ['id' => $condomino->id]) }}"
                                                            onsubmit="return confirm('Tem certeza que deseja remover {{ addslashes( $condomino->nome )}}?')">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" title="Remover"
                                                                class="btn btn-danger btn-link btn-sm">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </form>
                                                    </span>
                                                    @endif

                                                </span>
                                            </td>
                                        </tr>


                                        @endforeach



                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{!! asset('js/terceiros/DataTables/js/jquery.dataTables.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/components/DataTableCompontent.js') !!}"></script>
<script type="text/javascript">
	let dataTableComponent = new DataTableCompontent('#condominos');
	dataTableComponent.init();
</script>
@endsection