@extends('layouts.app', ['activePage' => $data->activePage, 'titlePage' => __($data->titlePage)])

<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">

            @include('components.messages_feedback')

            <div class="col-md-12">

                <div class="card ">
                    <div class="card-header card-header-rose card-header-text">
                        <div class="card-text">
                            <h4 class="card-title">{{ $data->cardTitle}}</h4>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('save_condomino') }}" class="form-horizontal"
                            enctype='multipart/form-data'>
                            @csrf

                            @if(isset($data->id) && $data->id!=0)
                            <input type="hidden" id="txtId" name="txtId" value="{{ $data->id }}">
                            @method('PUT')
                            @endif

                            <input type="hidden" class="form-control " id="selUnidade" name="selUnidade" value="1">

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Nome</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control " id="txtNome" name="txtNome"
                                        maxlength="200" value="{{ old('txtNome') ? old('txtNome') : $data->nome}}">
                                        <span class="bmd-help">Nome Completo.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Email</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="email" class="form-control " id="txtEmail" name="email"
                                        maxlength="200" value="{{ old('txtEmail') ? old('txtEmail') : $data->email}}">
                                        <span class="bmd-help">Email.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Tipo de Pessoa</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <select class="custom-select" name="selTipoPessoa" 
                                            onchange="condominoController.onchangeSelTipoPessoa()"
                                            id="selTipoPessoa">
                                            @php
                                                $selected = old('selTipoPessoa') ? old('selTipoPessoa') :
                                                $data->tipo_morador_id;

                                                $divCpf = "none";
                                                $divCnpj = "none";
                                                if ($selected==1){
                                                    $divCpf = "";
                                                    $divCnpj = "none";
                                                }elseif($selected==2){
                                                    $divCpf = "none";
                                                    $divCnpj = "";
                                                }
                                            @endphp

                                            <option disabled selected>Escolha...</option>
                                            @foreach($tipoPessoas as $tipoPessoa)
                                            <option {{ $selected == $tipoPessoa->id ? 'selected' : '' }}
                                                value="{{$tipoPessoa->id}}">{{$tipoPessoa->nome}}
                                            </option>

                                            @endforeach


                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="divCpf" style="display:{{$divCpf}}">
                                <label class="col-sm-2 col-form-label obrigatorio">Cpf</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control campo-cpf" id="txtCpf" maxlength="14" name="txtCpf"
                                            value="{{ old('txtCpf') ? old('txtCpf') : $data->cpf}}">
                                        <span class="bmd-help">Cpf.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="divCnpj" style="display:{{$divCnpj}}">
                                <label class="col-sm-2 col-form-label obrigatorio">Cnpj</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control campo-cnpj" id="txtCnpj" maxlength="18" name="txtCnpj"
                                            value="{{ old('txtCnpj') ? old('txtCnpj') : $data->cnpj}}">
                                        <span class="bmd-help">Cnpj.</span>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Tel. Celular</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control campo-celular" id="txtTelCelular" maxlength="15" name="txtTelCelular"
                                            value="{{ old('txtTelCelular') ? old('txtTelCelular') : $data->tel_celular}}">
                                        <span class="bmd-help">Tefone Celular com DDD.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Tel. Fixo</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control campo-telefone" id="txtTelFixo" maxlength="14" name="txtTelFixo"
                                            value="{{ old('txtTelFixo') ? old('txtTelFixo') : $data->tel_fixo}}">
                                        <span class="bmd-help">Tefone Fixo com DDD.</span>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Sexo</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <select class="custom-select" name="selSexo" id="selSexo">
                                            @php
                                            $selected = old('selSexo') ? old('selSexo') : $data->sexo ;
                                            @endphp

                                            <option disabled selected>Escolha...</option>
                                            <option {{ $selected == 'M' ? ' selected' : '' }} value="M">Masculino</option>
                                            <option {{ $selected == 'F' ? ' selected' : '' }} value="F">Feminino</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Tipo de Morador</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <select class="custom-select" name="selTipoMorador"
                                            id="selTipoMorador">
                                            @php
                                            $selected = old('selTipoMorador') ? old('selTipoMorador') :
                                            $data->tipo_morador_id;
                                            @endphp

                                            <option disabled selected>Escolha...</option>
                                            @foreach($tipoMoradores as $tipoMorador)
                                            <option {{ $selected == $tipoMorador->id ? 'selected' : '' }}
                                                value="{{$tipoMorador->id}}">{{$tipoMorador->nome}}
                                            </option>

                                            @endforeach


                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-footer ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                                    <a role="button" href="{{ route('condomino') }}"
                                        class="btn btn-light">{{ __('Voltar') }}</a>
                                </div>
                            </div>





                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('scripts')

<script type="text/javascript" src="{!! asset('js/helpers/StringUtil.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/controllers/CondominoController.js') !!}"></script>
<script type="text/javascript">
    let condominoController = new CondominoController();
</script>
@endsection