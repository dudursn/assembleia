<?php use App\Configs\TipoMoradorConfig; ?>

@extends('layouts.app', ['activePage' => 'tipo_morador', 'titlePage' => __('Tipo de Moradores')])

<link rel="stylesheet" type="text/css" href="{!! asset('js/terceiros/DataTables/css/jquery.dataTables.min.css') !!}">


@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="container-fluid">

        <div class="col-sm-10">
            <div class="form-group bmd-form-group">
                <a href="{{ route('create_tipo_morador') }}" role="button" class="btn btn-info" >
                    <span class="material-icons">add</span>
                    <span>{{ TipoMoradorConfig::titleNew() }}</span>
                </a>

            </div>
        </div>

        @include('components.messages_feedback')

      <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">{{ TipoMoradorConfig::titleCardPageIndex() }}</h4>
                        <p class="card-category">{{ TipoMoradorConfig::categoryCardPageIndex() }}</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tipoMoradores" class="table display">
                                <thead class=" text-primary">
									@foreach ($tableThLabels as $label)
                                    <th>
                                        {{ $label }}
                                    </th>
								 
									@endforeach
                                    <th class="td-actions text-right">
                                        Ações
                                    </th>

                                </thead>
                                <tbody>

									@foreach ($tipoMoradores as $tipoMorador)

                                    <tr>

                                        <td><a href="{{ route('edit_tipo_morador', $tipoMorador->id) }}"> {{ $tipoMorador->id}} </a></td>
                                        
                                        <td class="text-primary"><a href="{{ route('edit_tipo_morador', $tipoMorador->id) }}">{{ $tipoMorador->nome}} </a></td>

                                        <td class="text-right">
                                            <span>
                                                
                                                <span>
                                                    <a  role="button" href="{{ route('edit_tipo_morador', $tipoMorador->id) }}" title="Editar" class="btn btn-primary btn-link btn-sm">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                </span>
                                                

                                                @if (Auth::user() && Auth::user()->admin)
                                                <span>
                                                    <form  method="post" action="{{ route('delete_tipo_morador', ['id' => $tipoMorador->id]) }}"
                                                        onsubmit="return confirm('Tem certeza que deseja remover {{ addslashes( $tipoMorador->nome )}}?')">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" title="Remover"  class="btn btn-danger btn-link btn-sm">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                    </form>
                                                </span>
                                                @endif
                                            
                                            </span>
                                        </td>
                                    </tr>
									@endforeach
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{!! asset('js/terceiros/DataTables/js/jquery-3.5.1.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/terceiros/DataTables/js/jquery.dataTables.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/components/DataTableCompontent.js') !!}"></script>
<script type="text/javascript">
	let dataTableComponent = new DataTableCompontent('#tipoMoradores');
	dataTableComponent.init();
</script>
@endsection