

@extends('layouts.app', ['activePage' => $data->activePage, 'titlePage' => __($data->titlePage)])

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">

            @include('components.messages_feedback')
          
            <div class="col-md-12">

                <div class="card ">
                    <div class="card-header card-header-rose card-header-text">
                        <div class="card-text">
                            <h4 class="card-title">{{ $data->cardTitle}}</h4>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('save_tipo_morador') }}" class="form-horizontal"
                            enctype='multipart/form-data'>
                            @if(isset($data->id) && $data->id!=0)
                                <input type="hidden" id="txtId" name="txtId" value="{{ $data->id }}">
                                @method('PUT')
                            @endif

                            @csrf
                            
                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Nome</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control" id="txtNome" name="txtNome"
                                            value="{{ old('txtNome') ? old('txtNome') : $data->nome}}">
                                        <span class="bmd-help">Nome do Tipo de Pessoa.</span>
                                    </div>
                                </div>
                            </div>

                           

                            <div class="card ">

                                <div class="card-footer ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                                    <a role="button" href="{{ route('tipo_morador') }}" class="btn btn-light">{{ __('Voltar') }}</a>

                                </div>
                               
                            </div>





                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection