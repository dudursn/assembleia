@extends('layouts.app', ['activePage' => $data->activePage, 'titlePage' => __($data->titlePage)])

<link href="{{ asset('css/upload-file.css') }}" rel="stylesheet" />
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">

            @include('components.messages_feedback')

            <div class="col-md-12">

                <div class="card ">
                    <div class="card-header card-header-rose card-header-text">
                        <div class="card-text">
                            <h4 class="card-title">{{ $data->cardTitle}}</h4>
                        </div>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('save_assembleia') }}" class="form-horizontal"
                            enctype='multipart/form-data'>
                            @csrf

                            @if(isset($data->id) && $data->id!=0)
                            <input type="hidden" id="txtId" name="txtId" value="{{ $data->id }}">
                            @method('PUT')
                            @endif

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Categoria</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <select class="custom-select" name="selCategoria" id="selCategoria">
                                            @php
                                            $selected = old('selCategoria') ? old('selCategoria') : $data->categoria ;
                                            @endphp

                                            <option disabled selected>Escolha...</option>
                                            <option {{ $selected == 'O' ? ' selected' : '' }} value="O">Assembleia
                                                Ordinária</option>
                                            <option {{ $selected == 'E' ? ' selected' : '' }} value="E">Assembleia
                                                Extraordinária</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Situação da Assembleia</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <select class="custom-select" name="selSituacaoAssembleia"
                                            id="selSituacaoAssembleia">
                                            @php
                                            $selected = old('selSituacaoAssembleia') ? old('selSituacaoAssembleia') :
                                            $data->situacao_assembleia_id;
                                            @endphp

                                            <option disabled selected>Escolha...</option>
                                            @foreach($situacoesAssembleias as $situacaoAssembleia)
                                            <option {{ $selected == $situacaoAssembleia->id ? 'selected' : '' }}
                                                value="{{$situacaoAssembleia->id}}">{{$situacaoAssembleia->nome}}
                                            </option>

                                            @endforeach


                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label for="txtDataInicio" class="col-sm-2 col-form-label obrigatorio">Data
                                    Início</label>
                                <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="date"
                                            value="{{ old('txtDataInicio') ? old('txtDataInicio') : $data->data_inicio }}"
                                            id="txtDataInicio" name="txtDataInicio">

                                    </div>
                                </div>

                                <label for="txtDataEncerramento" class="col-sm-2 col-form-label obrigatorio">Data
                                    Encerramento</label>
                                <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="date"
                                            value="{{ old('txtDataEncerramento') ? old('txtDataEncerramento') : $data->data_fim }}"
                                            id="txtDataEncerramento" name="txtDataEncerramento">

                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <label for="txtHoraInicio" class="col-sm-2 col-form-label obrigatorio">Hora
                                    Início</label>
                                <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="time"
                                            value="{{ old('txtHoraInicio') ? old('txtHoraInicio') : $data->hora_inicio }}"
                                            id="txtHoraInicio" name="txtHoraInicio">

                                    </div>
                                </div>

                                <label for="txtHoraEncerramento" class="col-sm-2 col-form-label obrigatorio">Hora
                                    Encerramento</label>
                                <div class="col-sm-4">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="time"
                                            value="{{ old('txtHoraEncerramento') ? old('txtHoraEncerramento') : $data->hora_fim }}"
                                            id="txtHoraEncerramento" name="txtHoraEncerramento">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Link</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" class="form-control" id="txtLink" name="txtLink"
                                            value="{{ old('txtLink') ? old('txtLink') : $data->link }}">
                                        <span class="bmd-help">Link onde acontecerá a reunião online.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Observações</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <textarea class="form-control" id="txtObservacoes" name="txtObservacoes"
                                            rows="3" placeholder="">
                                                {{ old('txtObservacoes') ? old('txtObservacoes') : $data->observacoes }}
                                        </textarea>

                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <label class="col-sm-2 col-form-label obrigatorio">Pautas da Assembleia</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#verticalcenter" data-more="#sh" aria-pressed="false">
                                            <span class="material-icons">add</span>
                                            <span>Nova Pauta</span>
                                        </button>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Anexos</label>
                                <div class="col-sm-10">
                                    
                                        <div class="service-wrap">
                                            <div class="multiple-file-upload-wrap multiple-file-upload">
                                                <div class="already-uploaded-files">
                                                    <div class="aup-title">Arquivos: </div>

                                                    @if(isset($data->arquivos) && count($data->arquivos)>0)
                                                        @foreach ($data->arquivos as $arquivo)
                                                            <div class="aup-item forDelete">
                                                                <span class="aup-file-name">{{$arquivo->nome}}</span>
                                                                <span class="aup-delete-btn mfuDeleteBtn" onclick="assembleiaController.removeFile(event)"
                                                                    data-url="{{route('delete_json_arquivo', $arquivo->id)}}"
                                                                    data-send="{{$arquivo->id}}">Remover</span>
                                                                <div class="aup-server-response server-response"></div>
                                                                <div class="mfu-clear"></div>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <p>Nenhum arquivo adicionado para essa assembleia.</p>
                                                    @endif


                                                </div>
                                                <div id="mfu-items-container">
                                                    <div class="mfu-hide" id="mfu-item-template">
                                                        <div class="mfu-item">

                                                            <input class="mfu-file-input" type="file" name="files[]"
                                                                id="x-{ITEM_NUMBER}" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">

                                                    <div class="mfu-new-item-btn btn btn-info" id="mfu-new-item-btn">
                                                        Adicionar mais</div>
                                                    <div class="mfu-clear"></div>
                                                </div>
                                            </div>
                                        </div>

                                  
                                </div>
                            </div>


                            <div class="card">
                                <div class="card-footer ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                                    <a role="button" href="{{ route('assembleia') }}"
                                        class="btn btn-light">{{ __('Voltar') }}</a>
                                </div>
                            </div>





                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components.modals.modal-pautas')
@endsection



@section('scripts')
<script type="text/javascript" src="{!! asset('js/controllers/ArquivoController.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/controllers/AssembleiaController.js') !!}"></script>
<script type="text/javascript">
    let assembleiaController = new AssembleiaController();
    assembleiaController.init();
</script>
@endsection