<?php use App\Configs\AssembleiaConfig; ?>

@extends('layouts.app', ['activePage' => 'assembleia', 'titlePage' => __('Assembleias')])

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">
            @if (Auth::user() && Auth::user()->admin)
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <a href="{{ route('create_assembleia') }}" role="button" class="btn btn-info">
                        <span class="material-icons">add</span>
                        <span>{{ AssembleiaConfig::titleNew() }}</span>
                    </a>

                </div>
            </div>

            @include('components.messages_feedback')
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ AssembleiaConfig::titleCardPageIndex() }}</h4>
                            <p class="card-category">{{ AssembleiaConfig::categoryCardPageIndex() }}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">

							@foreach ($assembleias as $assembleia)

                                <div class="col-md-4">
                                    <div class="card card-outline-inverse">
                                        <div class="card-header">
                                            <h4 class="m-b-0 text-white">{{ $assembleia->categoria_str }}</h4>
                                        </div>
                                        <div class="card-body" style="border: 1px solid #000;">
                                        
                                            <p class="card-text">
												<label style="color:black;"><strong>{{$tableThLabels[0]}}:</strong> {{$assembleia->data_fim_str}}</label><br>
												<label style="color:black;">{{$tableThLabels[1]}}: {{$assembleia->data_inicio_str}}</label><br>
												<label style="color:black;">{{$tableThLabels[4]}}: {{$assembleia->situacaoAssembleia->nome}}</label>
											</p>
                                            @if (Auth::user() && Auth::user()->admin)
                                            <a href="{{ route('edit_assembleia', $assembleia->id) }}" class="btn btn-sm btn-primary">
												<span class="material-icons">edit</span> Editar
											</a>
                                            @endif
											<a href="#" class="btn btn-sm btn-info">
                                                <span class="material-icons">add</span> Detalhes
                                            </a>
                                        </div>
                                    </div>
								</div>

							@endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 