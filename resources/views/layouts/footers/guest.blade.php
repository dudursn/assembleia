<footer class="footer">
    <div class="container">
        <div class="col-md-6 ml-auto mr-auto text-center">
            <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script> - {{ config('constants.APP_NOME') }} by
            <a href="https://dudursn.github.io" target="_blank">Eduardo R S Nascimento.
            </div>
        </div>
    </div>
</footer>