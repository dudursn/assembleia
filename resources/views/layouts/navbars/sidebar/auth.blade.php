<div class="sidebar" data-color="orange" data-background-color="white"
    data-image="{{ asset('material') }}/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-normal">
            {{ __('Página Inicial') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#assembleiaDiv" aria-expanded="true">
                    <i class="material-icons">rss_feed</i>
                    <p>{{ __('Assembleias') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ $activePage == 'assembleia' ? ' show' : '' }} 
					{{ ($activePage == 'create_assembleia' ) ? ' show' : '' }}" id="assembleiaDiv">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'assembleia' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('assembleia') }}">
                                <i class="material-icons">format_list_bulleted</i>
                                <span class="sidebar-normal">{{ __('Listar') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'create_assembleia' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('create_assembleia') }}">
                                <i class="material-icons">note_add</i>
                                <span class="sidebar-normal">{{ __('Criar') }} </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="nav-item {{ $activePage == 'condomino' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('condomino') }}">
                    <i class="material-icons">people_alt</i>
                    <span class="sidebar-normal"> {{ __('Condôminos') }} </span>
                </a>
            </li>

            <li class="nav-item {{ $activePage == 'torre' ? ' active' : '' }}">
                <a class="nav-link " href="{{ route('torre') }}">
                    <i class="material-icons">apartment</i>
                    <span class="sidebar-normal">{{ __('Torres') }} </span>
                </a>
            </li>

            <li class="nav-item {{ $activePage == 'unidade' ? ' active' : '' }}">
                <a class="nav-link " href="{{ route('unidade') }}">
                    <i class="material-icons">house</i>
                    <span class="sidebar-normal">{{ __('Unidades') }} </span>
                </a>
            </li>

            <li class="nav-item {{ $activePage == 'usuario' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('usuario') }}">
                    <i class="material-icons">person</i>
                    <span class="sidebar-normal"> {{ __('Usuários') }} </span>
                </a>
            </li>
                        
            <li class="nav-item">
                <a class="nav-link " data-toggle="collapse" href="#tabelasBasicas" aria-expanded="true">
                    <i class="material-icons">storage</i>
                    <p>{{ __('Tabelas Básicas') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="tabelasBasicas">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('tipo_pessoa') }}">
                                <span class="sidebar-mini"> TP </span>
                                <span class="sidebar-normal">{{ __('Tipo Pessoas') }} </span>
                            </a>
                            <a class="nav-link " href="{{ route('tipo_morador') }}">
                                <span class="sidebar-mini"> TM </span>
                                <span class="sidebar-normal">{{ __('Tipo Moradores') }} </span>
                            </a>
                            <a class="nav-link " href="{{ route('situacao_assembleia') }}">
                                <span class="sidebar-mini"> SA </span>
                                <span class="sidebar-normal">{{ __('Situação Assembleia') }} </span>
                            </a>

                        </li>
                    </ul>
                </div>
            </li>



            <li class="nav-item">
                <a class="nav-link " data-toggle="collapse" href="#laravelExample" aria-expanded="true">
                    <i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>
                    <p>{{ __('Laravel Examples') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="laravelExample">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('profile.edit') }}">
                                <span class="sidebar-mini"> UP </span>
                                <span class="sidebar-normal">{{ __('Perfil do usuário') }} </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('user.index') }}">
                                <span class="sidebar-mini"> UM </span>
                                <span class="sidebar-normal"> {{ __('User Management') }} </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('blank') }}">
                                <span class="sidebar-mini"> B </span>
                                <span class="sidebar-normal"> {{ __('Blank') }} </span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('form') }}">
                                <span class="sidebar-mini"> F </span>
                                <span class="sidebar-normal"> {{ __('Forms') }} </span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('table') }}">
                                <i class="material-icons">content_paste</i>
                                <p>Table List</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('icons') }}">
                                <span class="sidebar-mini"> F </span>
                                <span class="sidebar-normal"> {{ __('Icons') }} </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>








        </ul>
    </div>
</div>