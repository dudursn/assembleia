<div class="wrapper ">
  @include('layouts.navbars.sidebar.auth')
  <div class="main-panel">
    @include('layouts.navbars.navs.auth')
    @yield('content')
    @include('layouts.footers.auth')
  </div>
</div>