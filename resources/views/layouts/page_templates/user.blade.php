<div class="wrapper ">
  @include('layouts.navbars.sidebar.user')
  <div class="main-panel">
    @include('layouts.navbars.navs.user')
    @yield('content')
    @include('layouts.footers.auth')
  </div>
</div>