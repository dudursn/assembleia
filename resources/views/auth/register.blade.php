@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'register', 'title' =>
config('constants.APP_NOME')])

@section('content')
<div class="container" style="height: auto;">
    <div class="row align-items-center">
        <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
            <form class="form" method="POST" action="{{ route('register') }}">
                @csrf

                <div class="card card-login card-hidden mb-3">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title"><strong>{{ __('Registre-se') }}</strong></h4>

                    </div> 
                    <div class="card-body ">

                        <div class="bmd-form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">face</i>
                                    </span>
                                </div>
                                <input type="text" name="name" class="form-control" placeholder="{{ __('Nome...') }}"
                                    value="{{ old('name') }}" required>
                            </div>
                            @if ($errors->has('name'))
                            <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                                <strong>{{ $errors->first('name') }}</strong>
                            </div>
                            @endif
                        </div>
                        <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">email</i>
                                    </span>
                                </div>
                                <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}"
                                    value="{{ old('email') }}" required>
                            </div>
                            @if ($errors->has('email'))
                            <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                            @endif
                        </div>

						<div class="bmd-form-group{{ $errors->has('selTipoPessoa') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">people</i>
                                    </span>
                                </div>

								<div class="form-group bmd-form-group">
									<select class="custom-select" name="selTipoPessoa"  onchange="condominoController.onchangeSelTipoPessoa()"
										id="selTipoPessoa">

								
										<option disabled selected>Tipo de Pessoa...</option>
										<option value="1">Pessoa Física
										</option>
										<option value="2">Pessoa Jurídica
										</option>

										<?php 
										$divCpf = "none";
										$divCnpj = "none";										
										/*
										@php
										$selected = old('selTipoPessoa') ? old('selTipoPessoa') :
										$data->tipo_morador_id;
										@endphp

										<option disabled selected>Escolha...</option>
										@foreach($tipoMoradores as $tipoMorador)
										<option {{ $selected == $tipoMorador->id ? 'selected' : '' }}
											value="{{$tipoMorador->id}}">{{$tipoMorador->nome}}
										</option>

										@endforeach
										*/
										?>

									</select>
								</div>
							</div>
							@if ($errors->has('selTipoPessoa'))
                            <div id="name-error" class="error text-danger pl-3" for="selTipoPessoa" style="display: block;">
                                <strong>{{ $errors->first('selTipoPessoa') }}</strong>
                            </div>
                            @endif
						</div>

						<div id="divCpf" style="display:{{$divCpf}}" 
							class="bmd-form-group{{ $errors->has('txtCpf') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">article</i>
                                    </span>
                                </div>

								<input type="text" class="form-control campo-cpf" id="txtCpf" maxlength="14"
									name="txtCpf" placeholder="{{ __('CPF...') }}"
									value="<?php /* {{ old('txtCpf') ? old('txtCpf') : $data->cpf}} */ ?>{{ old('txtCpf') }}" >
							
							</div>
							@if ($errors->has('txtCpf'))
                            <div id="name-error" class="error text-danger pl-3" for="txtCpf" style="display: block;">
                                <strong>{{ $errors->first('txtCpf') }}</strong>
                            </div>
                            @endif
						</div>

						<div id="divCnpj" style="display:{{$divCnpj}}"
							class="bmd-form-group{{ $errors->has('txtCnpj') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">article</i>
                                    </span>
                                </div>

								<input type="text" class="form-control campo-cnpj" id="txtCnpj" maxlength="14"
									name="txtCnpj" placeholder="{{ __('CNPJ...') }}"
									value="<?php /* {{ old('txtCnpj') ? old('txtCnpj') : $data->cpf}} */ ?>{{ old('txtCnpj') }}" >
										
								
							</div>

							@if ($errors->has('txtCnpj'))
                            <div id="name-error" class="error text-danger pl-3" for="txtCnpj" style="display: block;">
                                <strong>{{ $errors->first('txtCnpj') }}</strong>
                            </div>
                            @endif
						</div>

						<div class="bmd-form-group{{ $errors->has('txtTelCelular') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">smartphone</i>
                                    </span>
                                </div>

								<input type="text" class="form-control campo-celular" id="txtTelCelular" maxlength="15"
									name="txtTelCelular" placeholder="{{ __('Tel. Celular...') }}"
									value="<?php /* {{ old('txtTelCelular') ? old('txtTelCelular') : $data->cpf}} */ ?>{{ old('txtTelCelular') }}" >

							</div>

							@if ($errors->has('txtTelCelular'))
                            <div id="name-error" class="error text-danger pl-3" for="txtTelCelular" style="display: block;">
                                <strong>{{ $errors->first('txtTelCelular') }}</strong>
                            </div>
                            @endif
						</div>

						<div class="bmd-form-group{{ $errors->has('txtTelFixo') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">phone</i>
                                    </span>
                                </div>

								<input type="text" class="form-control campo-telefone" id="txtTelFixo" maxlength="14"
									name="txtTelFixo" placeholder="{{ __('Tel. Fixo...') }}"
									value="<?php /* {{ old('txtTelFixo') ? old('txtTelFixo') : $data->cpf}} */ ?>{{ old('txtTelFixo') }}" >								
							</div>

							@if ($errors->has('txtTelFixo'))
                            <div id="name-error" class="error text-danger pl-3" for="txtTelFixo" style="display: block;">
                                <strong>{{ $errors->first('txtTelFixo') }}</strong>
                            </div>
                            @endif
						</div>

						
						<div class="bmd-form-group{{ $errors->has('selTipoMorador') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">apartment</i>
                                    </span>
                                </div>

								<div class="form-group bmd-form-group">
									<select class="custom-select" name="selTipoMorador"
										id="selTipoMorador">
										<option disabled selected>Tipo de Morador...</option>
										<option value="1">Proprietário
										</option>
										<option value="2">Morador
										</option>
										<?php /*
										@php
										$selected = old('selTipoMorador') ? old('selTipoMorador') :
										$data->tipo_morador_id;
										@endphp

										<option disabled selected>Escolha...</option>
										@foreach($tipoMoradores as $tipoMorador)
										<option {{ $selected == $tipoMorador->id ? 'selected' : '' }}
											value="{{$tipoMorador->id}}">{{$tipoMorador->nome}}
										</option>

										@endforeach
										*/
										?>

									</select>
								</div>
							</div>
							@if ($errors->has('selTipoMorador'))
                            <div id="name-error" class="error text-danger pl-3" for="selTipoMorador" style="display: block;">
                                <strong>{{ $errors->first('selTipoMorador') }}</strong>
                            </div>
                            @endif
						</div>

						

						<?php /*
							<label class="col-sm-2 col-form-label obrigatorio">Tipo de Pessoa</label>
							<div class="col-sm-10">
								<div class="form-group bmd-form-group">
									<select class="custom-select" name="selTipoPessoa" 
										onchange="condominoController.onchangeSelTipoPessoa()"
										id="selTipoPessoa">

										
										@php
											$selected = old('selTipoPessoa') ? old('selTipoPessoa') :
											$data->tipo_morador_id;

											$divCpf = "none";
											$divCnpj = "none";
											if ($selected==1){
												$divCpf = "";
												$divCnpj = "none";
											}elseif($selected==2){
												$divCpf = "none";
												$divCnpj = "";
											}
										@endphp

										<option disabled selected>Escolha...</option>
										@foreach($tipoPessoas as $tipoPessoa)
										<option {{ $selected == $tipoPessoa->id ? 'selected' : '' }}
											value="{{$tipoPessoa->id}}">{{$tipoPessoa->nome}}
										</option>

										@endforeach

									
									</select>
								</div>
							</div>
						</div>


                    
						
						<div class="row">
							<label class="col-sm-2 col-form-label obrigatorio">Sexo</label>
							<div class="col-sm-10">
								<div class="form-group bmd-form-group">
									<select class="custom-select" name="selSexo" id="selSexo">
										
										@php
										$selected = old('selSexo') ? old('selSexo') : $data->sexo ;
										@endphp

										<option disabled selected>Escolha...</option>
										<option {{ $selected == 'M' ? ' selected' : '' }} value="M">Masculino</option>
										<option {{ $selected == 'F' ? ' selected' : '' }} value="F">Feminino</option>
									
									</select>
								</div>
							</div>
						</div>
						*/?>
  
                        <div class="bmd-form-group{{ $errors->has('Senha') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">lock_outline</i>
                                    </span>
                                </div>
                                <input type="password" name="password" id="password" class="form-control"
                                    placeholder="{{ __('Senha...') }}" required>
                            </div>
                            @if ($errors->has('password'))
                            <div id="password-error" class="error text-danger pl-3" for="password"
                                style="display: block;">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                            @endif
                        </div>
                        <div
                            class="bmd-form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">lock_outline</i>
                                    </span>
                                </div>
                                <input type="password" name="password_confirmation" id="password_confirmation"
                                    class="form-control" placeholder="{{ __('Confirme sua senha...') }}" required>
                            </div>
                            @if ($errors->has('password_confirmation'))
                            <div id="password_confirmation-error" class="error text-danger pl-3"
                                for="password_confirmation" style="display: block;">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </div>
                            @endif
                        </div>
                        <div class="form-check mr-auto ml-3 mt-3">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" id="policy" name="policy"
                                    {{ old('policy', 1) ? '' : 'checked' }}>
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                                {{ __('Eu não sou um robô ') }}
                            </label>
                        </div>
                    </div>
                    <div class="card-footer justify-content-center">
                        <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Criar conta') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="{!! asset('js/helpers/StringUtil.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/controllers/CondominoController.js') !!}"></script>
<script type="text/javascript">
let condominoController = new CondominoController();
</script>
@endsection