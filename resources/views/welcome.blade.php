@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'title' => config('constants.APP_NOME')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-7 col-md-8">
          <h1 class="text-white text-center">{{ __('Bem vindo ao Sistema de Votação.') }}</h1>
      </div>
  </div>
</div>
@endsection
