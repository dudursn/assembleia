<?php

//Aprendendo a criar constantes
return [
   
    'URL_NOME' => 'assembleia',
    'APP_NOME' => 'Sistema de Votação',
    'DATA' => '/' . $_SERVER['DOCUMENT_ROOT'] . '//data//',
    'options' => [
        'teste' => '123'
    ]
    
];

/*
Para Acessá-las
    Config::get('constants');
    Config::get('constants.options');
or if you want a specific one
    Config::get('constants.APP_NOME');
    Config::get('constants.options.teste');
*/