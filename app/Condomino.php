<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condomino extends Model
{
    
    //Define quais atributos são preenchíveis
    protected $fillable = ["nome", "email", "cpf", "cnpj", "sexo","tel_fixo", "tel_celular", "unidade_id","tipo_pessoa_id","tipo_morador_id"];

    //Relacionamento OneToOne - Um condomino pertence a uma unidade
    public function unidade(){
        
        return $this->belongsTo(Unidade::class);
    }

    public function usuario(){
        
        return $this->hasOne(User::class);
    }

    //Relacionamento OneToOne - Um condomino pertence a um tipo de morador (Proprietário ou Morador)
    public function tipoMorador(){
        
        return $this->belongsTo(TipoMorador::class);
    }

    //Relacionamento OneToOne - Um condomino pertence a um tipo de pessoa (Jurídica ou Física)
    public function tipoPessoa(){
        
        return $this->belongsTo(TipoPessoa::class);
    }
}
