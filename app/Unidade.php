<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    protected $fillable = ["numero", "nome_completo", "fracao"];

    protected $appends = ['full_name'];

    //Relacionamento OneToOne - Uma unidade pertence a uma torre
    public function torre(){
        
        return $this->belongsTo(Torre::class);
    }

    //Relacionamento OneToMany - Uma unidade tem varios condominos
    public function condominos(){

        return $this->hasMany(Condomino::class);
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute() {

        return "{$this->numero} - {$this->torre->nome}";
    }

}