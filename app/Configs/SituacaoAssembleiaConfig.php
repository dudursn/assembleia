<?php

namespace App\Configs;

use Illuminate\Http\Request;
use App\Interfaces\TableInterface;

class SituacaoAssembleiaConfig implements TableInterface
{
    public static function tableThLabels(){

        return ['Id', 'Nome'];
    }

    public static function titleCardPageIndex(){
        return "Situações de Assembleias";
    }

    public static function categoryCardPageIndex(){
        return "Lista de situações de assembleias cadastradas";
    }

    public static function titleNew(){
        return "Cadastrar Novo Situação de Assembleia";
    }
}
