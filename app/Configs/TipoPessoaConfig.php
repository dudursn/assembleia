<?php

namespace App\Configs;

use Illuminate\Http\Request;
use App\Interfaces\TableInterface;

class TipoPessoaConfig implements TableInterface
{
    public static function tableThLabels(){

        return ['Id', 'Nome'];
    }

    public static function titleCardPageIndex(){
        return "Tipos de Pessoas";
    }

    public static function categoryCardPageIndex(){
        return "Lista de tipos de pessoas cadastradas";
    }

    public static function titleNew(){
        return "Cadastrar Novo Tipo de Pessoa";
    }

}
