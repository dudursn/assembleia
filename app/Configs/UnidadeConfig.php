<?php

namespace App\Configs;

use Illuminate\Http\Request;
use App\Interfaces\TableInterface;

class UnidadeConfig implements TableInterface
{
    public static function tableThLabels(){

        return ["Id", "Unidade", "Fração Ideal", "Torre"];
    }

    public static function titleCardPageIndex(){
        return "Unidades";
    }

    public static function categoryCardPageIndex(){
        return "Lista de unidades cadastradas";
    }

    public static function titleNew(){
        return "Cadastrar Nova Unidade";
    }
}
