<?php

namespace App\Configs;

use Illuminate\Http\Request;
use App\Interfaces\TableInterface;

class TorreConfig implements TableInterface
{
    public static function tableThLabels(){

        return ['Id', 'Nome'];
    }

    public static function titleCardPageIndex(){
        return "Torres";
    }

    public static function categoryCardPageIndex(){
        return "Lista de torres cadastradas";
    }

    public static function titleNew(){
        return "Cadastrar Nova Torre";
    }
}
