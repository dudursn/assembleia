<?php

namespace App\Configs;

use Illuminate\Http\Request;
use App\Interfaces\TableInterface;

class AssembleiaConfig implements TableInterface
{
    public static function tableThLabels(){

        return ["Data de Encerramento", "Data de Início", "Hora de Encerramento", "Hora de Início", "Situação"];
    }

    public static function titleCardPageIndex(){
        return "Assembleias";
    }

    public static function categoryCardPageIndex(){
        return "Lista de assembleias cadastradas";
    }

    public static function titleNew(){
        return "Cadastrar Nova Assembleia";
    }
}
