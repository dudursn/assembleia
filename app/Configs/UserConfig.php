<?php

namespace App\Configs;

use Illuminate\Http\Request;
use App\Interfaces\TableInterface;

class UserConfig implements TableInterface
{
    public static function tableThLabels(){

        return ['Id', 'Nome', 'Email', 'Tipo de Usuário'];
    }

    public static function titleCardPageIndex(){
        return "Usuários";
    }

    public static function categoryCardPageIndex(){
        return "Lista de usuários cadastrados";
    }

    public static function titleNew(){
        return "Cadastrar Novo Condomino-Usuário";
    }
}
