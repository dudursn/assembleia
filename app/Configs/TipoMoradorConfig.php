<?php

namespace App\Configs;

use Illuminate\Http\Request;
use App\Interfaces\TableInterface;

class TipoMoradorConfig implements TableInterface
{
    public static function tableThLabels(){

        return ['Id', 'Nome'];
    }

    public static function titleCardPageIndex(){
        return "Tipos de Moradores";
    }

    public static function categoryCardPageIndex(){
        return "Lista de tipos de moradores cadastrados";
    }

    public static function titleNew(){
        return "Cadastrar Novo Tipo de Morador";
    }
}
