<?php

namespace App\Configs;

use Illuminate\Http\Request;
use App\Interfaces\TableInterface;

class CondominoConfig implements TableInterface
{
    public static function tableThLabels(){

        return ["Id", "Nome", "Email", "Pessoa", "Tipo de Morador", "Unidade"];
    }

    public static function titleCardPageIndex(){
        return "Condominos";
    }

    public static function categoryCardPageIndex(){
        return "Lista de condominos cadastrados";
    }

    public static function titleNew(){
        return "Cadastrar Novo Condomino";
    }
}
