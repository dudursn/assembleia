<?php

namespace App\Interfaces;



interface TableInterface
{
    public static function tableThLabels();
    public static function titleCardPageIndex();
    public static function categoryCardPageIndex();
    
}
