<?php

namespace App\Interfaces;



interface DaosInterface
{
    public function save($dados);
    public function update($dados);
    public function delete(int $id) :array;
    public function findById(int $id, $completo, $carregarColecao);
    public function findAll($completo, $carregarColecao);    
}
