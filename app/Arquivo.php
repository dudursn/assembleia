<?php

namespace App;

use App\Assembleia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Arquivo extends Model
{
   

    protected $fillable = ["nome", "extensao", "path", "assembleia_id"];

    //Relacionamento OneToOne - Um arquivo pertence a uma assembleia
    public function assembleia(){
        
        return $this->belongsTo(Assembleia::class);
    }
    // Outros relacionamentos belongsTo, hasMany, hasOne, belongsToMany

    
    
}
