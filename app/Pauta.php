<?php

namespace App;

use App\Assembleia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Pauta extends Model
{
   

    protected $fillable = ["assunto", "descricao", "data_inicio", "data_fim", "hora_inicio", "hora_fim"];

    //Relacionamento OneToOne - Uma temporada pertence a uma série
    public function assembleia(){
        
        return $this->belongsTo(Assembleia::class);
    }

    
    
}
