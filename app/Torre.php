<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Torre extends Model
{
    //Define quais atributos são preenchíveis
    protected $fillable = ["nome"];

    //Relacionamento OneToMany - Uma torre tem varias unidades
    public function unidades(){

        return $this->hasMany(Unidade::class);
    }

}
