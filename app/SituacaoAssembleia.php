<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituacaoAssembleia extends Model
{
    protected $table = 'situacoes_assembleias';

    protected $fillable = ["nome"];

     //Relacionamento OneToMany - Um situacao tem varias assembleias
     public function assembleias(){

        return $this->hasMany(Assembleia::class);
    }


}
