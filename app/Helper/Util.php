<?php

namespace App\Helper;

class Util {

    public static function arrayAsString($array, $separador=",") {
        return implode($separador, $array);
    }


/*     public static function emailValido($email) {
        $conta = "/^[a-zA-Z0-9\._-]+@";
        $domino = "[a-zA-Z0-9\._-]+.";
        $extensao = "([a-zA-Z]{2,4})$/";
        $pattern = $conta.$domino.$extensao;
        if (preg_match($pattern, $email) == 1) {
            return true;
        }
        else {
            return false;
        }
    } */

    public static function emailValido($email) {
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        
        if (preg_match($pattern, $email) == 1) {
            return true;
        }
        return false;
    }

    public static function emailConfValido($email, $emailConf){

        if(trim($email)==trim($emailConf)){
            return true;
        }
        
        return false;
    }

    public static function anoValido($ano){

        $pattern = "/\d{4}/";
        if(preg_match($pattern, trim($ano))){
            return true;
        }
        
        return false;
    }


  

     public static function limparMascara($valor, $caracteres) {

        $aux = $valor;
        
        for($i = 0; $i < strlen($caracteres); $i++){
            $aux = str_replace($caracteres[$i],"", $aux);
        }
        return $aux;
    }


     public static function mask($valor, $mascara) {
        $valorFormatado = '';
        $k = 0;
        if($valor!=''){
            for ($i = 0; $i < strlen($mascara); $i++) {
                if ($mascara[$i] == '#') {
                    if (isset($valor[$k]))
                        $valorFormatado .= $valor[$k];
                        $k++;
                } else {
                    if (isset($mascara[$i]))
                        $valorFormatado .= $mascara[$i];
                }
            }
        }
        return $valorFormatado;
    }
    
    
    

    public static function camelCaseToHyphen($string) {

        $retorno = "";
        for ($i = 0; $i < strlen($string); $i++) {
        
            if ($string[$i] == mb_strtolower($string[$i], "UTF-8")) {
                $retorno .= $string[$i];
            }
            else {
                $retorno .= "-" . mb_strtolower($string[$i], "UTF-8");
            }
        }
        return $retorno;
    }

    public static function mb_ucfirst($string) {
       
        $string = trim($string);
        return trim( mb_strtoupper( mb_substr($string, 0, 1) ).mb_strtolower( mb_substr($string, 1) ) );  
    }


    public static function removeEspacoEmBranco($string) {
        
        $stripped = "";
        if($string){
            $stripped = preg_replace('/\s/',"", $string);
        }

        return $stripped;
    }

    public static function tamanhoMaximoBytes($size){

        $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
        return $size ? ceil(round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) ) . $filesizename[$i] : '0 Bytes';
    }
}