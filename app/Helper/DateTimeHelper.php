<?php

namespace App\Helper;

class DateTimeHelper
{   
    
    public static function comparar($data1, $data2)
    {
        $d1 = "" . self::formatarDataServidor($data1);
        $d2 = "" . self::formatarDataServidor($data2);

        if ($d1 == $d2) {
            return 0;
        } else if ($d1 < $d2) {
            return 1;
        } else {
            return -1;
        }
    }

    public static function compararHora($hora1, $hora2)
    {

        if ($hora1 == $hora2) {
            return 0;
        } else if ($hora1 < $hora2) {
            return 1;
        } else {
            return -1;
        }
    }


    public static function dataValida($data)
    {

        $retorno = 0;

        if (substr_count($data, '/') == 2) {
            $d = explode("/", $data);
            $dia = $d[0];
            $mes = $d[1];
            $ano = $d[2];

            $retorno = checkdate($mes, $dia, $ano);
        }
        return $retorno;
    }


    public static function datasNoIntervalo($data1, $data2)
    {

        $d1 = date("" . self::formatarDataServidor($data1));
        $d2 = date("" . self::formatarDataServidor($data2));

        $datas = array();

        $data = $d1;
        while (self::comparar($data, $d2) > -1) {

            $datas[] = $data;
            $data = date("Y-m-d", strtotime("+1 day", strtotime($data)));
        }

        return $datas;
    }


    public static function formatarDataCliente($data)
    {

        $d = explode("-", $data);

        if (count($d) == 3) {

            $ano = $d[0];
            $mes = $d[1];
            $dia = $d[2];

            return $dia . '/' . $mes . '/' . $ano;
        }
        return $data;
    }


    public static function formatarDataClienteDiaMes($data)
    {

        if($data!=null && $data!=""){

            $d = explode("-", $data);

            if (count($d) == 3) {

                $mes = $d[1];
                $dia = $d[2];

                return $dia . '/' . $mes;
            }
        }
        return $data;
    }


    public static function formatarDataServidor($data)
    {
        if($data!=null && $data!=""){
            $d = explode("/", $data);

            if (count($d) == 3) {

                $dia = $d[0];
                $mes = $d[1];
                $ano = $d[2];

                return $ano . '-' . $mes . '-' . $dia;
            }
        }

        return $data;
    }


    public static function getDiaDaSemana($data)
    {

        if (count(explode("-", $data)) == 3) {

            $diaDaSemana = array("Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado");
            $numeroDiaDaSemana = date("w", strtotime(date($data)));
            return  $diaDaSemana[$numeroDiaDaSemana];
        }
        return "";
    }


    public static function horaNoIntervalo(
        $horaInicialAtividade,
        $horaFinalAtividade,
        $horaInicialOcupada,
        $horaFinalOcupada,
        $inclusive = true
    ) {
        if ($inclusive) {

            if (($horaInicialAtividade <= $horaFinalOcupada) && ($horaFinalAtividade >= $horaInicialOcupada)
            ) {

                return true;
            }
        } else {

            if (($horaInicialAtividade < $horaFinalOcupada) && ($horaFinalAtividade > $horaInicialOcupada)
            ) {

                return true;
            }
        }

        return false;
    }

    public static function horaValida($hora)
    {

        $retorno = 0;

        if (substr_count($hora, ':') == 1) {
            $h = explode(":", $hora);
            $hora = $h[0];
            $minuto = $h[1];

            if (($hora >= 0 && $hora <= 23) && ($minuto >= 0 && $minuto <= 59)) {
                $retorno = 1;
            }
        }
        return $retorno;
    }

    public static function totalDeSegundos(String $tempo) {

        list($horas,$mins,$seg) = explode(":",$tempo); //removemos as divisões do tempo

        $segundos = 0;
    
        if($horas <> 0){
            for($i=0;$i<$horas;$i++){ // uma hora = 3600s então, contaremos de 3600 em 3600
                $segundos = $segundos + 3600;
            }
        }
        if($mins <> 0){
            for($j=0;$j<$mins;$j++){//o mesmo do anterior, 1 minuto = 60s contaremos de 60 em 60
                $segundos = $segundos + 60;
            }
        }
        $total = $segundos + $seg; //somamos tudo e temos o total de tempo em segundos.

        return $total;
    }

    public static function formataDataCompleta($data)
    {
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        return strftime('%d de %B de %Y', strtotime(date($data)));
    }

    public static function dataHojeCompleta()
    {
        return self::formataDataCompleta(date('m/d/Y'));
    }

    public static function anoAtual()
    {
        return date('Y');
    }

    public static function formataDataAno($data){
        $date = new DateTime($data);
        return $date->format('Y');
    }

    public static function formataHoraMinuto($tempo){
        $time = new DateTime($tempo);
        return $time->format('H:i');
    }
}
