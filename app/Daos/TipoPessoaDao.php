<?php

namespace App\Daos;

use App\{TipoPessoa, Condomino};
use Illuminate\Support\Facades\DB;
use App\Interfaces\DaosInterface;

class TipoPessoaDao implements DaosInterface{

    public function save($dados) : TipoPessoa {

        $tipoPessoa = null;
        DB::beginTransaction();

        $tipoPessoa = TipoPessoa::create([
            'nome' => trim(mb_strtoupper($dados['txtNome']))
        ]);
    

        DB::commit();

        return $tipoPessoa;
    }

    public function update($dados) {
      
        $tipoPessoa = TipoPessoa::find($dados['txtId']);
        if($tipoPessoa!= null){

		    $tipoPessoa->fill([
                "nome" =>  trim(mb_strtoupper($dados['txtNome']))
            ]);

            $tipoPessoa->save();
        }
        return $tipoPessoa;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $tipoPessoa = TipoPessoa::find($id);
        if($tipoPessoa!=null){

            $tipoPessoa = $this->carregarDados($tipoPessoa,  false, true); 
            $resposta["nome"] = $tipoPessoa->nome;
           
            if(count($tipoPessoa->condominos)>0){
                
                $resposta["msg"] = "Não é possível apagar a {$tipoPessoa->nome}, pois ela possui condominos. " .
                        "Apague antes todas as condominos cadastradas com essa tipoPessoa";
            }else{
                
                $tipoPessoa->delete(); 
            }   
           
        }else{
            $resposta["msg"] = "Erro ao remover a TipoPessoa.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$tipoPessoas = TipoPessoa::whereRaw("1 = 1")
           ->orderBy('nome')->get(); */
        $tipoPessoas = TipoPessoa::all();
        
        $tipoPessoas->each(function(TipoPessoa $tipoPessoa) use ( $completo, $carregarColecao){
            
            $tipoPessoa = $this->carregarDados($tipoPessoa,  $completo, $carregarColecao);          
       });

       return $tipoPessoas;
   }

   public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $tipoPessoas = TipoPessoa::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $tipoPessoas = TipoPessoa::paginate($perPage);

        return $tipoPessoas;
   }

   public function findById(int $id, $completo=false, $carregarColecao=false){

        $tipoPessoa = $this->carregarDados(TipoPessoa::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $tipoPessoa;
   }

   private function carregarDados($tipoPessoa, $completo, $carregarColecao){

        if($tipoPessoa!=null){
           
            if($completo){

            }

            if($carregarColecao){
             
                $tipoPessoa->condominos = Condomino::query()
                        ->where('tipo_pessoa_id', $tipoPessoa->id)
                        ->orderBy('nome')->get();  

                
            }
        }

        return $tipoPessoa;     
   }

    
}