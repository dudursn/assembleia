<?php

namespace App\Daos;

use App\{TipoMorador, Condomino};
use Illuminate\Support\Facades\DB;
use App\Interfaces\DaosInterface;

class TipoMoradorDao implements DaosInterface{

    public function save($dados) : TipoMorador {

        $tipoMorador = null;
        DB::beginTransaction();

        $tipoMorador = TipoMorador::create([
            'nome' => trim(mb_strtoupper($dados['txtNome']))            
        ]);
    

        DB::commit();

        return $tipoMorador;
    }

    public function update($dados) {
      
        $tipoMorador = TipoMorador::find($dados['txtId']);
        if($tipoMorador!= null){

		    $tipoMorador->fill([
                "nome" =>  trim(mb_strtoupper($dados['txtNome']))
            ]);

            $tipoMorador->save();
        }
        return $tipoMorador;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $tipoMorador = TipoMorador::find($id);
        if($tipoMorador!=null){

            $tipoMorador = $this->carregarDados($tipoMorador,  false, true); 
            $resposta["nome"] = $tipoMorador->nome;
           
            if(count($tipoMorador->condominos)>0){
                
                $resposta["msg"] = "Não é possível apagar a {$tipoMorador->nome}, pois ela possui condominos. " .
                        "Apague antes todas as condominos cadastradas com essa tipoMorador";
            }else{
                
                $tipoMorador->delete(); 
            }   
           
        }else{
            $resposta["msg"] = "Erro ao remover a TipoMorador.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$tipoMoradors = TipoMorador::whereRaw("1 = 1")
           ->orderBy('nome')->get(); */
        $tipoMoradors = TipoMorador::all();
        
        $tipoMoradors->each(function(TipoMorador $tipoMorador) use ( $completo, $carregarColecao){
            
            $tipoMorador = $this->carregarDados($tipoMorador,  $completo, $carregarColecao);          
       });

       return $tipoMoradors;
   }

   public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $tipoMoradors = TipoMorador::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $tipoMoradors = TipoMorador::paginate($perPage);

        return $tipoMoradors;
   }

   public function findById(int $id, $completo=false, $carregarColecao=false){

        $tipoMorador = $this->carregarDados(TipoMorador::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $tipoMorador;
   }

   private function carregarDados($tipoMorador, $completo, $carregarColecao){

        if($tipoMorador!=null){
           
            if($completo){

            }

            if($carregarColecao){
             
                $tipoMorador->condominos = Condomino::query()
                        ->where('tipo_pessoa_id', $tipoMorador->id)
                        ->orderBy('nome')->get();  

                
            }
        }

        return $tipoMorador;     
   }

    
}