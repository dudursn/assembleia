<?php

namespace App\Daos;

use App\Helper\Util;
use App\{Condomino, User};
use App\Interfaces\DaosInterface;
use Illuminate\Support\Facades\DB;

class CondominoDao implements DaosInterface{

    public function save($dados) : Condomino {

        $condomino = null;
        DB::beginTransaction();

        $condomino = Condomino::create($this->fillData($dados));
    

        DB::commit();

        return $condomino;
    }

    public function update($dados) {
      
        $condomino = Condomino::find($dados['txtId']);
        if($condomino!= null){

		    $condomino->fill($this->fillData($dados));

            $condomino->save();
        }
        return $condomino;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $condomino = Condomino::find($id);
        if($condomino!=null){

            $condomino = $this->carregarDados($condomino,  false, true); 
            $resposta["nome"] = $condomino->nome;
            
         
            if($condomino->usuario!= null){
                
                $resposta["msg"] = "Não é possível apagar o registro {$condomino->nome}. Ele é um usuário cadastro. Apague antes o usuario.";
            }else{
                
                $condomino->delete(); 
            }   
           
        }else{
            $resposta["msg"] = "Erro ao remover o registro.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$condominos = Condomino::whereRaw("1 = 1")
           ->orderBy('nome')->get(); */
        $condominos = Condomino::all();
        
        $condominos->each(function(Condomino $condomino) use ( $completo, $carregarColecao){
            
            $condomino = $this->carregarDados($condomino,  $completo, $carregarColecao);          
       });

       return $condominos;
   }

   public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $condominos = Condomino::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $condominos = Condomino::paginate($perPage);

        return $condominos;
   }

   public function findById(int $id, $completo=false, $carregarColecao=false){

        $condomino = $this->carregarDados(Condomino::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $condomino;
   }

    private function carregarDados($condomino, $completo, $carregarColecao){

        if($condomino!=null){
           
            if($completo){

            }

            if($carregarColecao){
             
             

                
            }
        }

        return $condomino;     
    }

    private function fillData($dados){

        return [
            "nome" => trim(mb_strtoupper($dados["txtNome"])),
            "email" => trim($dados["email"]),
            "cpf" => Util::limparMascara( trim($dados["txtCpf"]), ".-"),
            "cnpj" =>  Util::limparMascara( trim($dados["txtCnpj"]), ".- /"),
            "sexo" => trim($dados["selSexo"]),
            "tel_fixo" => Util::limparMascara( trim($dados["txtTelFixo"]), "()- " ),
            "tel_celular" => Util::limparMascara( trim($dados["txtTelCelular"]),  "()- " ),
            "tipo_pessoa_id" => trim($dados["selTipoPessoa"]),
            "tipo_morador_id" => trim($dados["selTipoMorador"]),
            "unidade_id" => trim($dados["selUnidade"])
        ];


    }

    
}