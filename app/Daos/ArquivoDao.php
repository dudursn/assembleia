<?php

namespace App\Daos;

use App\{Arquivo, Assembleia};
use Illuminate\Support\Facades\DB;
use App\Interfaces\DaosInterface;

class ArquivoDao implements DaosInterface{

    public function save($dados) : Arquivo {

        $arquivo = null;
        DB::beginTransaction();

        $arquivo = Arquivo::create([
            "nome" => $dados["txtNome"],
            "extensao" => $dados["txtExtensao"],
            "path" => $dados["txtPath"],
            "assembleia_id" => $dados["txtAssembleiaId"]
        ]);
    

        DB::commit();

        return $arquivo;
    }

    public function update($dados) {
      
        $arquivo = Arquivo::find($dados["txtId"]);
        if($arquivo!= null){

		    $arquivo->fill([
                "nome" => $dados["txtNome"],
                "extensao" => $dados["txtExtensao"],
                "path" => $dados["txtPath"],
                "assembleia_id" => $dados["txtAssembleiaId"]
            ]);

            $arquivo->save();
        }
        return $arquivo;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $arquivo = Arquivo::find($id);
        if($arquivo!=null){

            $arquivo = $this->carregarDados($arquivo,  false, true); 
            $resposta["nome"] = $arquivo->nome;
       
            $arquivo->delete();    
        }else{
            $resposta["msg"] = "Erro ao remover a Arquivo.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$arquivos = Arquivo::whereRaw("1 = 1")
           ->orderBy('numero')->get(); */
        $arquivos = Arquivo::all();
        
        $arquivos->each(function(Arquivo $arquivo) use ( $completo, $carregarColecao){
            
            $arquivo = $this->carregarDados($arquivo,  $completo, $carregarColecao);          
       });

       return $arquivos;
   }

   public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $arquivos = Arquivo::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $arquivos = Arquivo::paginate($perPage);

        return $arquivos;
   }

   public function findById(int $id, $completo=false, $carregarColecao=false){

        $arquivo = $this->carregarDados(Arquivo::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $arquivo;
   }

   private function carregarDados($arquivo, $completo, $carregarColecao){

        if($arquivo!=null){
           
            if($completo){
                
                $arquivo->assembleia = Assembleia::query()
                        ->where('id', $arquivo->assembleia_id)
                        ->orderBy('nome')->first();  
            }

            if($carregarColecao){
             
             

                
            }
        }

        return $arquivo;     
   }

    
}