<?php

namespace App\Daos;

use App\{User, Condomino};
use Illuminate\Support\Facades\DB;


class UserDao  {

  
    public function update($dados) {
      
        $usuario = User::find($dados["txtId"]);
        if($usuario!= null){

		    $usuario->fill([
                "admin" =>  trim(mb_strtoupper($dados["optAdmin"]))
            ]);

            $usuario->save();
        }
        return $usuario;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $usuario = User::find($id);
        if($usuario!=null){

            $usuario = $this->carregarDados($usuario,  false, true); 
            $resposta["nome"] = $usuario->nome;
           
            $usuario->delete(); 
                       
        }else{
            $resposta["msg"] = "Erro ao remover o Usuário.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$usuarios = User::whereRaw("1 = 1")
           ->orderBy("nome")->get(); */
        $usuarios = User::all();
        
        $usuarios->each(function(User $usuario) use ( $completo, $carregarColecao){
            
            $usuario = $this->carregarDados($usuario,  $completo, $carregarColecao);          
       });

       return $usuarios;
   }

   public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $usuarios = User::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $usuarios = User::paginate($perPage);

        return $usuarios;
   }

    public function findById(int $id, $completo=false, $carregarColecao=false){

        $usuario = $this->carregarDados(User::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $usuario;
    }

    public function findByCondominoId(int $condominoId, $completo=false, $carregarColecao=false){

        $usuario = $this->carregarDados(User::where("condomino_id", "=", $condominoId)->first(), $completo, $carregarColecao);
        return $usuario;
    }

   private function carregarDados($usuario, $completo, $carregarColecao){

        if($usuario!=null){
           
            if($completo){
                
                $usuario->condomino = Condomino::query()
                        ->where("usuario_id", $usuario->id)
                        ->orderBy("nome")->first();  
            }

            if($carregarColecao){
             
             
            }
        }

        return $usuario;     
   }

    
}