<?php

namespace App\Daos;

use App\{Unidade, Condomino, Torre};
use Illuminate\Support\Facades\DB;
use App\Interfaces\DaosInterface;

class UnidadeDao implements DaosInterface{

    public function save($dados) : Unidade {

        $unidade = null;
        DB::beginTransaction();

        $unidade = Unidade::create([
            "numero" => $dados["txtNumero"],
            "fracao" => $dados["txtFracao"],
            "torre_id" => $dados["selTorre"]
        ]);
    

        DB::commit();

        return $unidade;
    }

    public function update($dados) {
      
        $unidade = Unidade::find($dados["txtId"]);
        if($unidade!= null){

		    $unidade->fill([
                "numero" => $dados["txtNumero"],
                "fracao" => $dados["txtFracao"],
                "torre_id" => $dados["selTorre"]
            ]);

            $unidade->save();
        }
        return $unidade;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $unidade = Unidade::find($id);
        if($unidade!=null){

            $unidade = $this->carregarDados($unidade,  false, true); 
            $resposta["nome"] = $unidade->full_name;
           
            if(count($unidade->condominos)>0){
                
                $resposta["msg"] = "Não é possível apagar a unidade {$unidade->full_name}, pois ela possui condominos. " .
                        "Apague antes todos os condominos cadastrados com essa unidade";
            }else{
                
                $unidade->delete(); 
            }   
           
        }else{
            $resposta["msg"] = "Erro ao remover a Unidade.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$unidades = Unidade::whereRaw("1 = 1")
           ->orderBy('numero')->get(); */
        $unidades = Unidade::all();
        
        $unidades->each(function(Unidade $unidade) use ( $completo, $carregarColecao){
            
            $unidade = $this->carregarDados($unidade,  $completo, $carregarColecao);          
       });

       return $unidades;
   }

   public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $unidades = Unidade::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $unidades = Unidade::paginate($perPage);

        return $unidades;
   }

   public function findById(int $id, $completo=false, $carregarColecao=false){

        $unidade = $this->carregarDados(Unidade::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $unidade;
   }

   private function carregarDados($unidade, $completo, $carregarColecao){

        if($unidade!=null){
           
            if($completo){
                
                $unidade->torre = Torre::query()
                        ->where('unidade_id', $unidade->id)
                        ->orderBy('nome')->first();  
            }

            if($carregarColecao){
             
                $unidade->condominos = Condomino::query()
                        ->where('unidade_id', $unidade->id)
                        ->orderBy('nome')->get();  

                
            }
        }

        return $unidade;     
   }

    
}