<?php

namespace App\Daos;

use App\{SituacaoAssembleia, Assembleia};
use Illuminate\Support\Facades\DB;
use App\Interfaces\DaosInterface;

class SituacaoAssembleiaDao implements DaosInterface{

    public function save($dados) : SituacaoAssembleia {

        $situacaoAssembleia = null;
        DB::beginTransaction();

        $situacaoAssembleia = SituacaoAssembleia::create([
            'nome' => trim(mb_strtoupper($dados['txtNome']))
        ]);
    

        DB::commit();

        return $situacaoAssembleia;
    }

    public function update($dados) {
      
        $situacaoAssembleia = SituacaoAssembleia::find($dados['txtId']);
        if($situacaoAssembleia!= null){

		    $situacaoAssembleia->fill([
                "nome" =>  trim(mb_strtoupper($dados['txtNome'], "UTF-8"))
            ]);

            $situacaoAssembleia->save();
        }
        return $situacaoAssembleia;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $situacaoAssembleia = SituacaoAssembleia::find($id);
        if($situacaoAssembleia!=null){

            $situacaoAssembleia = $this->carregarDados($situacaoAssembleia,  false, true); 
            $resposta["nome"] = $situacaoAssembleia->nome;
           
            if(count($situacaoAssembleia->assembleias)>0){
                
                $resposta["msg"] = "Não é possível apagar a {$situacaoAssembleia->nome}, pois ela possui assembleias. " .
                        "Apague antes todas as assembleias cadastradas com essa situacaoAssembleia";
            }else{
                
                $situacaoAssembleia->delete(); 
            }   
           
        }else{
            $resposta["msg"] = "Erro ao remover a SituacaoAssembleia.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$situacaoAssembleias = SituacaoAssembleia::whereRaw("1 = 1")
           ->orderBy('nome')->get(); */
        $situacaoAssembleias = SituacaoAssembleia::all();
        
        $situacaoAssembleias->each(function(SituacaoAssembleia $situacaoAssembleia) use ( $completo, $carregarColecao){
            
            $situacaoAssembleia = $this->carregarDados($situacaoAssembleia,  $completo, $carregarColecao);          
       });

       return $situacaoAssembleias;
   }

   public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $situacaoAssembleias = SituacaoAssembleia::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $situacaoAssembleias = SituacaoAssembleia::paginate($perPage);

        return $situacaoAssembleias;
   }

   public function findById(int $id, $completo=false, $carregarColecao=false){

        $situacaoAssembleia = $this->carregarDados(SituacaoAssembleia::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $situacaoAssembleia;
   }

   private function carregarDados($situacaoAssembleia, $completo, $carregarColecao){

        if($situacaoAssembleia!=null){
           
            if($completo){

            }

            if($carregarColecao){
             
                $situacaoAssembleia->assembleias = Assembleia::query()
                        ->where('situacaoAssembleia_id', $situacaoAssembleia->id)
                        ->orderBy('numero')->get();  

                
            }
        }

        return $situacaoAssembleia;     
   }

    
}