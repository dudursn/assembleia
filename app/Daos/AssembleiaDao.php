<?php

namespace App\Daos;

use App\Helper\DateTimeHelper;
use Illuminate\Support\Facades\DB;
use App\Interfaces\DaosInterface;
use App\{Assembleia, Pauta, Arquivo};

class AssembleiaDao implements DaosInterface{

    public function save($dados) : Assembleia {

        $assembleia = null;
        DB::beginTransaction();

        $assembleia = Assembleia::create($this->fillData($dados));    

        DB::commit();

        return $assembleia;
    }

    public function update($dados) {
      
        $assembleia = Assembleia::find($dados["txtId"]);
        if($assembleia!= null){

		    $assembleia->fill($this->fillData($dados));

            $assembleia->save();
        }
        return $assembleia;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $assembleia = Assembleia::find($id);
        if($assembleia!=null){

            $assembleia = $this->carregarDados($assembleia,  false, true); 
            $resposta["nome"] = "Assembleia do Dia Inicial " . DateTimeHelper::formatarDataCliente($request->input('txtDataInicio'));
           
            if(count($assembleia->pautas)>0){
                
                $resposta["msg"] = "Não é possível apagar a ". $resposta["nome"] .", pois ela possui pautas. " .
                        "Apague antes todas as unidades cadastradas com essa assembleia";
            }else{
                
                $assembleia->delete(); 
            }   
           
        }else{
            $resposta["msg"] = "Erro ao remover a Assembleia.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$assembleias = Assembleia::whereRaw("1 = 1")
            ->orderBy('nome')->get(); */
        $assembleias = Assembleia::all();
        
        $assembleias->each(function(Assembleia $assembleia) use ( $completo, $carregarColecao){
            
            $assembleia = $this->carregarDados($assembleia,  $completo, $carregarColecao);          
        });

        return $assembleias;
    }

    public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $assembleias = Assembleia::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $assembleias = Assembleia::paginate($perPage);

        return $assembleias;
    }

    public function findById(int $id, $completo=false, $carregarColecao=false){

        $assembleia = $this->carregarDados(Assembleia::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $assembleia;
    }

    private function carregarDados($assembleia, $completo, $carregarColecao){

        if($assembleia!=null){
           
            if($completo){

            }

            if($carregarColecao){
             
                $assembleia->pautas = Pauta::query()
                        ->where("assembleia_id", $assembleia->id)
                        ->orderBy("numero")->get();  

                $assembleia->arquivos = Arquivo::query()
                        ->where("assembleia_id", $assembleia->id)
                        ->orderBy("nome")->get();  

                
            }
        }

        return $assembleia;     
    }    

    private function fillData($dados){

        return [
            "categoria" => $dados["selCategoria"],
            "data_inicio" => $dados["txtDataInicio"],
            "data_fim" => $dados["txtDataEncerramento"],
            "hora_inicio" => $dados["txtHoraInicio"],
            "hora_fim" => $dados["txtHoraEncerramento"],
            "situacao_assembleia_id" => $dados["selSituacaoAssembleia"],
            "link" => trim($dados["txtLink"]),
            "observacoes" => trim($dados["txtObservacoes"])
        ];


    }

   
}