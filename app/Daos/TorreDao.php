<?php

namespace App\Daos;

use App\{Torre, Unidade};
use Illuminate\Support\Facades\DB;
use App\Interfaces\DaosInterface;

class TorreDao implements DaosInterface{

    public function save($dados) : Torre {

        $torre = null;
        DB::beginTransaction();

        $torre = Torre::create([
            "nome" => trim(mb_strtoupper($dados["txtNome"]))
        ]);
    

        DB::commit();

        return $torre;
    }

    public function update($dados) {
      
        $torre = Torre::find($dados["txtId"]);
        if($torre!= null){

		    $torre->fill([
                "nome" =>  trim(mb_strtoupper($dados["txtNome"]))
            ]);

            $torre->save();
        }
        return $torre;
    }

    public function delete(int $id) : array 
    {
        $resposta = array(
            "nome" => "",
            "msg" => ""
        );

        DB::beginTransaction();

        $torre = Torre::find($id);
        if($torre!=null){

            $torre = $this->carregarDados($torre,  false, true); 
            $resposta["nome"] = $torre->nome;
           
            if(count($torre->unidades)>0){
                
                $resposta["msg"] = "Não é possível apagar a {$torre->nome}, pois ela possui unidades. " .
                        "Apague antes todas as unidades cadastradas com essa torre";
            }else{
                
                $torre->delete(); 
            }   
           
        }else{
            $resposta["msg"] = "Erro ao remover a Torre.";
        }

        DB::commit();
        
        return $resposta;
    }

    public function findAll($completo=false, $carregarColecao=false){

        /*$torres = Torre::whereRaw("1 = 1")
           ->orderBy("nome")->get(); */
        $torres = Torre::all();
        
        $torres->each(function(Torre $torre) use ( $completo, $carregarColecao){
            
            $torre = $this->carregarDados($torre,  $completo, $carregarColecao);          
       });

       return $torres;
   }

   public function findAllPorPagina(int $page, int $perPage){
        /*
        Forma 1
        $offset = ($page - 1) * $perPage;
        $torres = Torre::query()->offset( $offset )->limit($perPage)->get();
        */

        /*Forma 2 */
        $torres = Torre::paginate($perPage);

        return $torres;
   }

   public function findById(int $id, $completo=false, $carregarColecao=false){

        $torre = $this->carregarDados(Torre::where("id", "=", $id)->first(), $completo, $carregarColecao);
        return $torre;
   }

   private function carregarDados($torre, $completo, $carregarColecao){

        if($torre!=null){
           
            if($completo){

            }

            if($carregarColecao){
             
                $torre->unidades = Unidade::query()
                        ->where("torre_id", $torre->id)
                        ->orderBy("numero")->get();  

                
            }
        }

        return $torre;     
   }

    
}