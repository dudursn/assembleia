<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPessoa extends Model
{
    //
    protected $fillable = ["nome"];

     //Relacionamento OneToMany - Um tipo de pessoa tem varios condominos
     public function condominos(){

        return $this->hasMany(Condomino::class);
    }
}
