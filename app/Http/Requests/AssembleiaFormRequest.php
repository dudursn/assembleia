<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssembleiaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "selCategoria" => " required",
            "txtLink" => " required",
            "txtDataInicio"  =>  "required|date",
            "txtDataEncerramento"    =>  "required|date|after_or_equal:txtDataInicio",
            "txtHoraInicio"  =>  "required|date_format:H:i",
            "txtHoraEncerramento"    =>  "required|date_format:H:i|after:txtHoraInicio"

        ];
    }

     /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages ()
    {
        return [
            
            "selCategoria.required" => "O campo categoria é obrigatório",
            "txtLink.required" => "O campo link é obrigatório",
            "txtDataInicio.required" => "O campo data de início é obrigatório",
            "txtDataEncerramento.required" => "O campo data de encerramento é obrigatório",
            "date" => "Informe uma data válida",
            "after_or_equal" => "O campo data de encerramento deve ser uma data posterior ou igual a data de inicio",
            "txtHoraInicio.required" => "O campo hora de início é obrigatório",
            "txtHoraEncerramento.required" => "O campo hora de encerramento é obrigatório",
            "date_format:H:i" => "Informe um horário válido",
            "after" => "O campo hora de encerramento deve ser um horário posterior a hora de inicio"
            
            
        ];
    }
}
