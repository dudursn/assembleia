<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required', 'min:3'
            ],
            'email' => [
                'required', 'email', Rule::unique((new User)->getTable())->ignore($this->route()->user->id ?? null)
            ],
            'password' => [
                $this->route()->user ? 'nullable' : 'required', 'confirmed', 'min:8'
            ]
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages ()
    {
        return [
            'required' => 'Este campo é obrigatório',
            'name.min' => 'O campo nome precisa ter pelo menos três caracteres',
            'email' => "Informe um email válido",
            'email.unique' => 'Já existe um email cadastrado no sistema',
            'password.min' => 'Este campo precisa ter pelo menos oito caracteres'
        ];
    }
}
