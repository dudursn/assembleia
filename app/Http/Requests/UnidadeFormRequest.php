<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnidadeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "selTorre" => " required",
            "txtNumero" => " required | numeric | min:1",
           
        ];
    }

     /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages ()
    {
        return [
            
            "selTorre.required" => "O campo Torre é obrigatório",
            "txtNumero.required" => "O campo número da unidade é obrigatório",
            "txtNumero.numeric" => "O campo número da unidade somente aceita números",
            "txtNumero.min" => "O campo número da unidade tem que ser maior que zero"
            
            
        ];
    }
}
