<?php

namespace App\Http\Requests;

use App\Condomino;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CondominoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            
            "txtNome" => " required",
   
            "txtCpf" => "required_if:selTipoPessoa,1",
            "txtCnpj" => "required_if:selTipoPessoa,in:2",

            "email" =>  [
                'required', 'email'
            ],
            "selSexo" => "required",
            "txtTelCelular" => "required_without_all:txtTelFixo",
            "txtTelFixo" => " required_without_all:txtTelCelular",
            
            "selTipoMorador" => " required",
            "selTipoPessoa" => " required",
            //"selUnidade" => " required",

        ];

        if($this->input("selTipoPessoa")==1){
            $rules["txtCpf"] .= "|cpf";
        }

    
        if($this->input("selTipoPessoa")==2){
            $rules["txtCnpj"] .= "|cnpj";
        }

        if($this->input("txtTelCelular")){
            $rules["txtTelCelular"] .= "|celular_com_ddd";            
        }

        if($this->input("txtTelFixo")){
            $rules["txtTelFixo"] .= "|telefone_com_ddd";            
        }

        return $rules;

    }

     /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages ()
    {
        return [

            "txtNome.required" => "O campo nome é obrigatório",

            "email" => "Informe um email válido",
            "email.unique" => "Já existe um email cadastrado no sistema",

            "selTipoPessoa.required" => "O campo tipo de pessoa é obrigatório",

            "txtCpf.required_if" => "O campo cpf é obrigatório quando o tipo de pessoa for Física.",
            "txtCpf.cpf" => "Cpf inválido",

            "txtCnpj.required_if" => "O campo cnpj é obrigatório quando o tipo de pessoa for Jurídica.",
            "txtCnpj.cnpj" => "Cnpj inválido",

            "email.required" => "O campo email é obrigatório",

            "selSexo.required" => "O campo sexo é obrigatório",

            "txtTelCelular.required_without_all" => "Informe pelo menos um telefone",
            "txtTelCelular.celular_com_ddd" => "O campo Tel. Celular não é um celular com DDD válido",
            "txtTelFixo.required_without_all" => "Informe pelo menos um telefone",
            "txtTelFixo.telefone_com_ddd" => "O campo Tel. Fixo não é um celular com DDD válido",

            "selTipoMorador.required" => "O campo tipo de morador é obrigatório"            
        ];
    }

  
}
