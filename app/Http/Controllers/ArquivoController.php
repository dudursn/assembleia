<?php

namespace App\Http\Controllers;

use App\Arquivo;
use App\Daos\ArquivoDao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File ;
use Illuminate\Support\Facades\Storage;


class ArquivoController extends Controller
{
    public function index(Request $request){

        $arquivos = Arquivo::query()
            ->orderBy("nome")->get();
        
        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
        
        return view("pages.arquivo.index", compact("arquivos", "mensagem", "tipoAlert"));
   
    }

    public function create(){
	
		$data =  $this->fillData(null);	
    	return view("pages.arquivo.form", compact("data"));
	}
	
	public function edit($id, ArquivoDao $arquivoDao){
		$arquivo = $arquivoDao->findById($id, false, false);

		if($arquivo!=null){	

			$data =  $this->fillData($arquivo);		
			return view("pages.arquivo.form", compact("data"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Tipo de Morador não encontrada"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("arquivo");
    }
    
    public function store(int $assembleiaId, Request $request, ArquivoDao $arquivoDao){
      
        $this->validate($request, [
            "file" => "file|mimes:png,jpg,webp,gif,pdf, xps,odt,docx |max:20000"
        ]);

        $files = $request->file("files");
        if($files && count($files)>0){
            foreach ($files as $file){
                $dados = array(
                    "txtNome" => $file->getClientOriginalName(),
                    "txtExtensao" => $file->getClientOriginalExtension(),
                    "txtPath" => $file->store("uploads/assembleias/${assembleiaId}"),
                    "txtAssembleiaId" => $assembleiaId
                );

            $arquivoDao->save($dados);
            }	
        }
        
	}

	public function update(Request $request, ArquivoDao $arquivoDao){
    
		$dados = $request->input();
		$arquivo = $arquivoDao->update($dados);
	
		if($arquivo){

			$nome = $arquivo->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi atualizado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("arquivo");
		}
    }

    public function deleteJson(int $id, ArquivoDao $arquivoDao){
       
        $arquivo = $arquivoDao->findById($id, false, false);
        if(Storage::delete($arquivo->path)){

            $resposta = $arquivoDao->delete($id);

            if($resposta["msg"]==""){
                
                return response()->json([
                    "msg" => "Arquivo foi removido com sucesso!"
                ]);
            }
        }
        return response()->json([
            "error" => "Erro ao remover o arquivo. Contate ao administrador do sistema"
        ]);
    }

    public function destroy (Request $request, ArquivoDao $arquivoDao){
        if(Storage::delete($arquivo->path)){
            $resposta = $arquivoDao->delete($request->id);
        }else{
            $resposta["msg"] = "Erro ao remover o arquivo. Contate ao administrador do sistema";
        }
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("arquivo");
	}

}

