<?php

namespace App\Http\Controllers;

use App\TipoMorador;
use Illuminate\Http\Request;
use App\Configs\TipoMoradorConfig;
use App\Daos\TipoMoradorDao;
use App\Http\Requests\TipoMoradorFormRequest;

class TipoMoradorController extends Controller
{
    public function index(Request $request){

        $tipoMoradores = TipoMorador::query()
            ->orderBy('nome')->get();

        $tableThLabels = TipoMoradorConfig::tableThLabels();
        
        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
        
        return view("pages.tipo_morador.index", compact("tipoMoradores", "tableThLabels", "mensagem", "tipoAlert"));
   
    }

    public function create(){
	
		$data =  $this->fillData(null);	
    	return view("pages.tipo_morador.form", compact("data"));
	}
	
	public function edit($id, TipoMoradorDao $tipoMoradorDao){
		$tipoMorador = $tipoMoradorDao->findById($id, false, false);

		if($tipoMorador!=null){	

			$data =  $this->fillData($tipoMorador);		
			return view("pages.tipo_morador.form", compact("data"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Tipo de Morador não encontrada"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("tipo_morador");
    }
    
    public function store(TipoMoradorFormRequest $request, TipoMoradorDao $tipoMoradorDao){
		
		$dados = $request->input();
		$tipoMorador = $tipoMoradorDao->save($dados);
	
		if($tipoMorador){

			$nome = $tipoMorador->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi criado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("tipo_morador");
		}

		return redirect()->route("create_tipo_morador")->withInput();	
	}

	public function update(TipoMoradorFormRequest $request, TipoMoradorDao $tipoMoradorDao){
    
		$dados = $request->input();
		$tipoMorador = $tipoMoradorDao->update($dados);
	
		if($tipoMorador){

			$nome = $tipoMorador->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi atualizado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("tipo_morador");
		}
    }


    public function destroy (Request $request, TipoMoradorDao $tipoMoradorDao){
    
        $resposta = $tipoMoradorDao->delete($request->id);
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("tipo_morador");
	}

	private function fillData($tipoMorador){

		$data = (object)array();

		if($tipoMorador==null){
			
			$data->id = 0;
			$data->nome = "";
			$data->titlePage = "Novo Tipo de Morador";
			$data->activePage = "tipoMorador/criar";
			$data->cardTitle = "Novo";
		}else{
		
			$data = $tipoMorador;
			$data->titlePage = "Editar Tipo de Morador";
			$data->activePage = "tipoMorador/editar/{$tipoMorador->id}";
			$data->cardTitle = "Editar";
		}

		return $data;

	}
}

