<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use App\Helper\DateTimeHelper;

use App\Configs\AssembleiaConfig;
use App\Daos\{AssembleiaDao, SituacaoAssembleiaDao, ArquivoDao};
use App\{Assembleia,Pauta,Arquivo};
use App\Http\Controllers\Controller;
use App\Http\Controllers\ArquivoController;
use App\Http\Requests\AssembleiaFormRequest;

class AssembleiaController extends Controller
{

    public function index(Request $request){
		
        $assembleias = Assembleia::query()
            ->orderBy('data_fim', 'DESC')->get();

		$tableThLabels = AssembleiaConfig::tableThLabels();

        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
		
		return view( "pages.assembleia.index", compact("assembleias", "tableThLabels", "mensagem", "tipoAlert"));
    }

	public function create(SituacaoAssembleiaDao $situacaoAssembleiaDao){
	
		$data =  $this->fillData(null);	
		$situacoesAssembleias = $situacaoAssembleiaDao->findAll(false,false);

    	return view("pages.assembleia.form", compact("data", "situacoesAssembleias"));
	}
	
	public function edit($id, AssembleiaDao $assembleiaDao, SituacaoAssembleiaDao $situacaoAssembleiaDao){
		$assembleia = $assembleiaDao->findById($id, false, false);

		if($assembleia!=null){	

			$data =  $this->fillData($assembleia);	
			$situacoesAssembleias = $situacaoAssembleiaDao->findAll(false,false);

			return view("pages.assembleia.form", compact("data", "situacoesAssembleias"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Assembleia não encontrada"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("assembleia");
    }
    
	
	public function store(AssembleiaFormRequest $request, AssembleiaDao $assembleiaDao, 
		ArquivoController $arquivoController, ArquivoDao $arquivoDao){
		
		$dados = $request->input();
		$assembleia = $assembleiaDao->save($dados);
	
		if($assembleia){
			
			if($request->hasFile('files')){
				$arquivoController->store($assembleia->id, $request, $arquivoDao);
			}
			
			$nome = DateTimeHelper::formatarDataCliente($request->input('txtDataInicio'));

			$request->session()
				->flash(
					"mensagem",
					"Assembleia do Dia Inicial {$nome} foi criado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("assembleia");
			
		}

		return redirect()->route("create_assembleia")->withInput();
		
		
	}


	public function update(AssembleiaFormRequest $request, AssembleiaDao $assembleiaDao, 
		ArquivoController $arquivoController, ArquivoDao $arquivoDao){
    
		$dados = $request->input();
	
		$assembleia = $assembleiaDao->update($dados);
	
		if($assembleia){

			$arquivoController->store($assembleia->id, $request, $arquivoDao);

			$nome = DateTimeHelper::formatarDataCliente($request->input('txtDataInicio'));

			$request->session()
				->flash(
					"mensagem",
					"Assembleia do Dia Inicial {$nome} foi atualizado(a) com sucesso."
				);
		
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("assembleia");
		}
    }


    public function destroy (Request $request, AssembleiaDao $assembleiaDao){
    
        $resposta = $assembleiaDao->delete($request->id);
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("assembleia");
	}

	private function fillData($assembleia){

		$data = (object)array();

		if($assembleia==null){
			
			$data->id = 0;
			$data->categoria = "";
			$data->data_inicio = "";
			$data->data_fim = "";
			$data->hora_inicio = "";
			$data->hora_fim = "";
			$data->situacao_assembleia_id = "";
			$data->link = "";
			$data->observacoes = "";

			$data->titlePage = "Nova Assembleia";
			$data->activePage = "assembleia/criar";
			$data->cardTitle = "Novo";
		}else{
		
			$data = $assembleia;

			$data->titlePage = "Editar Assembleia";
			$data->activePage = "assembleia/editar/{$assembleia->id}";
			$data->cardTitle = "Editar";
		}

		return $data;

	}
}
