<?php

namespace App\Http\Controllers;

use App\User;
use App\Condomino;
use App\Daos\UserDao;
use App\Configs\UserConfig;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserFormRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * 
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $users = User::query()
            ->orderBy("name")->get();

        $tableThLabels = UserConfig::tableThLabels();

        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
        
        return view('pages.users.index', compact("users", "tableThLabels", "mensagem", "tipoAlert"));
        
    }

    public function edit($id, UserDao $userDao){

		$usuario = $userDao->findById($id, false, false);
	
		if($usuario!=null){	

			$data =  $this->fillData($usuario);	
		
			return view("pages.users.form", compact("data"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Usuário não encontrado"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("usuario");
    }

    public function update(UserFormRequest $request, UserDao $userDao, UserController $userController){
    
		$dados = $request->input();
		$usuario = $userDao->update($dados);
			
		if($usuario){

			$nome = $usuario->nome;

					$request->session()
				->flash(
					"mensagem",
					"{$nome} foi atualizado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("usuario");
		}
    }

    public function destroy (Request $request, UserDao $userDao){
    
        $resposta = $userDao->delete($request->id);
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("usuario");
	}

    public function saveCondominoInUser(Condomino $condomino){
        
        $randomPass = Str::random(8);
        $hashedRandomPassword = Hash::make($randomPass);

        return User::create([
            "name" => trim(mb_strtoupper($condomino->nome)),
            "email" => $condomino->email,
            "admin"=> "0",
            "condomino_id"=> $condomino->id,
            "password" => Hash::make($hashedRandomPassword),
        ]);
    }

    private function fillData($usuario){

		$data = (object)array();

		if($usuario!=null){
					
			$data = $usuario;
			$data->titlePage = "Editar Usuário";
			$data->activePage = "usuario/editar/{$usuario->id}";
			$data->cardTitle = "Editar";
		}

		return $data;

	}
}
