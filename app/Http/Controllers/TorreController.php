<?php

namespace App\Http\Controllers;

use App\Torre;
use App\Configs\TorreConfig;
use App\Daos\TorreDao;
use Illuminate\Http\Request;
use App\Http\Requests\TorreFormRequest;

class TorreController extends Controller
{
    public function index(Request $request)
    {
        $torres = Torre::query()
            ->orderBy('nome')->get();

        $tableThLabels = TorreConfig::tableThLabels();

        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
        
        return view('pages.torre.index', compact("torres", "tableThLabels", "mensagem", "tipoAlert"));
    }

    public function create(){
	
		$data =  $this->fillData(null);	
    	return view("pages.torre.form", compact("data"));
	}
	
	public function edit($id, TorreDao $torreDao){
		$torre = $torreDao->findById($id, false, false);

		if($torre!=null){	

			$data =  $this->fillData($torre);		
			return view("pages.torre.form", compact("data"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Torre não encontrada"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("torre");
    }
    
    public function store(TorreFormRequest $request, TorreDao $torreDao){
		
		$dados = $request->input();
		$torre = $torreDao->save($dados);
	
		if($torre){

			$nome = $torre->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi criado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("torre");
		}

		return redirect()->route("create_torre")->withInput();	
	}

	public function update(TorreFormRequest $request, TorreDao $torreDao){
    
		$dados = $request->input();
		$torre = $torreDao->update($dados);
	
		if($torre){

			$nome = $torre->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi atualizado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("torre");
		}
    }


    public function destroy (Request $request, TorreDao $torreDao){
    
        $resposta = $torreDao->delete($request->id);
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("torre");
	}

	private function fillData($torre){

		$data = (object)array();

		if($torre==null){
			
			$data->id = 0;
			$data->nome = "";
			$data->titlePage = "Nova Torre";
			$data->activePage = "torre/criar";
			$data->cardTitle = "Novo";
		}else{
		
			$data = $torre;
			$data->titlePage = "Editar Torre";
			$data->activePage = "torre/editar/{$torre->id}";
			$data->cardTitle = "Editar";
		}

		return $data;

	}
}
