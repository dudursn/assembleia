<?php

namespace App\Http\Controllers;

use App\SituacaoAssembleia;
use Illuminate\Http\Request;
use App\Configs\SituacaoAssembleiaConfig;
use App\Daos\SituacaoAssembleiaDao;
use App\Http\Requests\SituacaoAssembleiaFormRequest;

class SituacaoAssembleiaController extends Controller
{
    public function index(Request $request){

        $situacoesAssembleias = SituacaoAssembleia::query()
            ->orderBy('nome')->get();

        $tableThLabels = SituacaoAssembleiaConfig::tableThLabels();
        
        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
        
        return view("pages.situacao_assembleia.index", compact("situacoesAssembleias", "tableThLabels", "mensagem", "tipoAlert"));
   
    }

    public function create(){
	
		$data =  $this->fillData(null);	
    	return view("pages.situacao_assembleia.form", compact("data"));
	}
	
	public function edit($id, SituacaoAssembleiaDao $situacaoAssembleiaDao){
		$situacaoAssembleia = $situacaoAssembleiaDao->findById($id, false, false);

		if($situacaoAssembleia!=null){	

			$data =  $this->fillData($situacaoAssembleia);		
			return view("pages.situacao_assembleia.form", compact("data"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Situação de Assembleia não encontrado(a)"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("situacao_assembleia");
    }
    
    public function store(SituacaoAssembleiaFormRequest $request, SituacaoAssembleiaDao $situacaoAssembleiaDao){
		
		$dados = $request->input();
		$situacaoAssembleia = $situacaoAssembleiaDao->save($dados);
	
		if($situacaoAssembleia){

			$nome = $situacaoAssembleia->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi criado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("situacao_assembleia");
		}

		return redirect()->route("create_situacao_assembleia")->withInput();	
	}

	public function update(SituacaoAssembleiaFormRequest $request, SituacaoAssembleiaDao $situacaoAssembleiaDao){
    
		$dados = $request->input();
		$situacaoAssembleia = $situacaoAssembleiaDao->update($dados);
	
		if($situacaoAssembleia){

			$nome = $situacaoAssembleia->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi atualizado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("situacao_assembleia");
		}
    }


    public function destroy (Request $request, SituacaoAssembleiaDao $situacaoAssembleiaDao){
    
        $resposta = $situacaoAssembleiaDao->delete($request->id);
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("situacao_assembleia");
	}

	private function fillData($situacaoAssembleia){

		$data = (object)array();

		if($situacaoAssembleia==null){
			
			$data->id = 0;
			$data->nome = "";
			$data->titlePage = "Novo Situação de Assembleia";
			$data->activePage = "situacaoAssembleia/criar";
			$data->cardTitle = "Novo";
		}else{
		
			$data = $situacaoAssembleia;
			$data->titlePage = "Editar Situação de Assembleia";
			$data->activePage = "situacaoAssembleia/editar/{$situacaoAssembleia->id}";
			$data->cardTitle = "Editar";
		}

		return $data;

	}
}

