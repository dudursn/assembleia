<?php

namespace App\Http\Controllers;

use App\Condomino;
use Illuminate\Http\Request;
use App\Configs\CondominoConfig;
use App\Http\Controllers\UserController;
use App\Http\Requests\CondominoFormRequest;
use App\Daos\{CondominoDao, TipoPessoaDao, TipoMoradorDao};

class CondominoController extends Controller
{
    public function index(Request $request)
    {
        $condominos = Condomino::query()
            ->orderBy('nome')->get();

        $tableThLabels = CondominoConfig::tableThLabels();

        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
        
        return view('pages.condomino.index', compact("condominos", "tableThLabels", "mensagem", "tipoAlert"));
    }

    public function create(TipoMoradorDao $tipoMoradorDao, TipoPessoaDao $tipoPessoaDao){
	
		$data =  $this->fillData(null);	
        $tipoMoradores = $tipoMoradorDao->findAll(false,false);
        $tipoPessoas = $tipoPessoaDao->findAll(false,false);

    	return view("pages.condomino.form", compact("data", "tipoMoradores", "tipoPessoas"));
	}
	
	public function edit($id, CondominoDao $condominoDao, TipoMoradorDao $tipoMoradorDao, TipoPessoaDao $tipoPessoaDao){

		$condomino = $condominoDao->findById($id, false, false);
	
		if($condomino!=null){	

			$data =  $this->fillData($condomino);	
			$tipoMoradores = $tipoMoradorDao->findAll(false,false);
			$tipoPessoas = $tipoPessoaDao->findAll(false,false);
	
			return view("pages.condomino.form", compact("data", "tipoMoradores", "tipoPessoas"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Condomino não encontrado"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("condomino");
    }
    
    public function store(CondominoFormRequest $request, CondominoDao $condominoDao, UserController $userController){
		
		$dados = $request->input();
		$condomino = $condominoDao->save($dados);
	
		if($condomino){

			$nome = $condomino->nome;

			$userController->saveCondominoInUser($condomino);

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi criado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("condomino");
		}

		return redirect()->route("create_condomino")->withInput();	
	}

	public function update(CondominoFormRequest $request, CondominoDao $condominoDao, UserController $userController){
    
		$dados = $request->input();
		$condomino = $condominoDao->update($dados);
			
		if($condomino){

			$nome = $condomino->nome;

			if($condomino->usuario== null){
				$userController->saveCondominoInUser($condomino);
			}

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi atualizado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("condomino");
		}
    }


    public function destroy (Request $request, CondominoDao $condominoDao){
    
        $resposta = $condominoDao->delete($request->id);
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("condomino");
	}

	private function fillData($condomino){

		$data = (object)array();

		if($condomino==null){
			
            $data->id = 0;
            $data->nome = "";
            $data->email = "";
            $data->cnpj = "";
            $data->cpf = "";
            $data->sexo = "";
            $data->tel_fixo = "";
            $data->tel_celular = "";
            $data->unidade_id = "";
            $data->tipo_morador_id = "";
            $data->tipo_pessoa_id = "";
			
			$data->titlePage = "Novo Condômino";
			$data->activePage = "condomino/criar";
			$data->cardTitle = "Novo";
		}else{
		
			$data = $condomino;
			$data->titlePage = "Editar Condômino";
			$data->activePage = "condomino/editar/{$condomino->id}";
			$data->cardTitle = "Editar";
		}

		return $data;

	}
}
