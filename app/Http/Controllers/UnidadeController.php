<?php
namespace App\Http\Controllers;

use App\Unidade;
use App\Daos\TorreDao;
use App\Daos\UnidadeDao;
use Illuminate\Http\Request;
use App\Configs\UnidadeConfig;
use App\Http\Requests\UnidadeFormRequest;

class UnidadeController extends Controller
{
    public function index(Request $request)
    {
        $unidades = Unidade::query()
            ->orderBy('numero')->get();

        $tableThLabels = UnidadeConfig::tableThLabels();

        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
                
        return view('pages.unidade.index', compact("unidades", "tableThLabels", "mensagem", "tipoAlert"));
    }

    public function create(TorreDao $torreDao){
        
        $data =  $this->fillData(null);	
        $torres = $torreDao->findAll(false,false);
        
    	return view("pages.unidade.form", compact("data", "torres") );
	}
	
	public function edit($id, UnidadeDao $unidadeDao, TorreDao $torreDao){
        
		$unidade = $unidadeDao->findById($id, false, false);

		if($unidade!=null){	

            $data =  $this->fillData($unidade);		
            $torres = $torreDao->findAll(false,false);

			return view("pages.unidade.form", compact("data", "torres"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Unidade não encontrada"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("unidade");
    }
    
    public function store(UnidadeFormRequest $request, UnidadeDao $unidadeDao){
		
		$dados = $request->input();
		$unidade = $unidadeDao->save($dados);
	
		if($unidade){

			$nome = $unidade->full_name;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi criado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("unidade");
		}

		return redirect()->route("create_unidade")->withInput();	
	}

	public function update(UnidadeFormRequest $request, UnidadeDao $unidadeDao){
    
		$dados = $request->input();
		$unidade = $unidadeDao->update($dados);
	
		if($unidade){

			$nome = $unidade->full_name;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi atualizado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("unidade");
		}
    }


    public function destroy (Request $request, UnidadeDao $unidadeDao){
    
        $resposta = $unidadeDao->delete($request->id);
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("unidade");
	}

	private function fillData($unidade){

		$data = (object)array();

		if($unidade==null){
			
			$data->id = 0;
            $data->numero = "";
            $data->fracao = "";
            $data->torre_id = "";
			$data->titlePage = "Nova Unidade";
			$data->activePage = "unidade/criar";
			$data->cardTitle = "Novo";
		}else{
		
			$data = $unidade;
			$data->titlePage = "Editar Unidade";
			$data->activePage = "unidade/editar/{$unidade->id}";
			$data->cardTitle = "Editar";
		}

		return $data;

	}
}
