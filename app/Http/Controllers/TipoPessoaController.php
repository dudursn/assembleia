<?php

namespace App\Http\Controllers;

use App\TipoPessoa;
use Illuminate\Http\Request;
use App\Configs\TipoPessoaConfig;
use App\Daos\TipoPessoaDao;
use App\Http\Requests\TipoPessoaFormRequest;

class TipoPessoaController extends Controller
{
    public function index(Request $request){

        $tipoPessoas = TipoPessoa::query()
            ->orderBy('nome')->get();

        $tableThLabels = TipoPessoaConfig::tableThLabels();
        
        $mensagem = $request->session()->get("mensagem");
		$tipoAlert = $request->session()->get("tipoAlert");
        
        return view("pages.tipo_pessoa.index", compact("tipoPessoas", "tableThLabels", "mensagem", "tipoAlert"));
   
    }

    public function create(){
	
		$data =  $this->fillData(null);	
    	return view("pages.tipo_pessoa.form", compact("data"));
	}
	
	public function edit($id, TipoPessoaDao $tipoPessoaDao){
		$tipoPessoa = $tipoPessoaDao->findById($id, false, false);

		if($tipoPessoa!=null){	

			$data =  $this->fillData($tipoPessoa);		
			return view("pages.tipo_pessoa.form", compact("data"));
		}

		$request->session()
                ->flash(
                    "mensagem",
                    "Tipo de Pessoa não encontrado"
                );
            
		$request->session()
			->flash(
				"tipoAlert",
				"alert-danger"
			);
		return redirect()->route("tipo_pessoa");
    }
    
    public function store(TipoPessoaFormRequest $request, TipoPessoaDao $tipoPessoaDao){
		
		$dados = $request->input();
		$tipoPessoa = $tipoPessoaDao->save($dados);
	
		if($tipoPessoa){

			$nome = $tipoPessoa->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi criado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("tipo_pessoa");
		}

		return redirect()->route("create_tipo_pessoa")->withInput();	
	}

	public function update(TipoPessoaFormRequest $request, TipoPessoaDao $tipoPessoaDao){
    
		$dados = $request->input();
		$tipoPessoa = $tipoPessoaDao->update($dados);
	
		if($tipoPessoa){

			$nome = $tipoPessoa->nome;

			$request->session()
				->flash(
					"mensagem",
					"{$nome} foi atualizado(a) com sucesso."
				);
			//	$request->session()->put(...)

			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);

			return redirect()->route("tipo_pessoa");
		}
    }


    public function destroy (Request $request, TipoPessoaDao $tipoPessoaDao){
    
        $resposta = $tipoPessoaDao->delete($request->id);
     
		if($resposta["msg"]==""){

            $nome = $resposta["nome"];

			$request->session()
                ->flash(
                    "mensagem",
                    "{$nome} removido(a) com sucesso"
                );
            
			$request->session()
				->flash(
					"tipoAlert",
					"alert-success"
				);
		}else{

			$request->session()
                ->flash(
                    "mensagem",
                    $resposta["msg"]
                );

			$request->session()
				->flash(
					"tipoAlert",
					"alert-danger"
				);
		}

        return redirect()->route("tipo_pessoa");
	}

	private function fillData($tipoPessoa){

		$data = (object)array();

		if($tipoPessoa==null){
			
			$data->id = 0;
			$data->nome = "";
			$data->titlePage = "Novo Tipo de Pessoa";
			$data->activePage = "tipoPessoa/criar";
			$data->cardTitle = "Novo";
		}else{
		
			$data = $tipoPessoa;
			$data->titlePage = "Editar Tipo de Pessoa";
			$data->activePage = "tipoPessoa/editar/{$tipoPessoa->id}";
			$data->cardTitle = "Editar";
		}

		return $data;

	}
}

