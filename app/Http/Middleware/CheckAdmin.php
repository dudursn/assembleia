<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  int  $admin
     * @return mixed
     */
    public function handle($request, Closure $next, $admin)
    {
        if (! $request->user()->admin==$admin) {
            return abort(404);
        }

        return $next($request);
    }

}