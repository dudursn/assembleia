<?php

namespace App;

use App\{Pauta, Arquivo, SituacaoAssembleia};

use App\Helper\DateTimeHelper;
use Illuminate\Database\Eloquent\Model;

class Assembleia extends Model{


    //Define quais atributos são preenchíveis
    protected $fillable = ["categoria", "data_inicio", "data_fim", "hora_inicio", "hora_fim", "situacao_assembleia_id",
        "link", "tipo_voto","observacoes"];

    protected $appends = ['categoria_str, data_inicio_str, data_fim_str'];

    //Relacionamento OneToMany - Uma assembleia tem várias pautas
    public function pautas(){

        return $this->hasMany(Pauta::class);
    }

    public function arquivos(){

        return $this->hasMany(Arquivo::class);
    }

     //Relacionamento OneToOne - Uma assembleia pertence a uma situacao de assembleia
     public function situacaoAssembleia(){
        
        return $this->belongsTo(SituacaoAssembleia::class);
    }


     /**
     * Get the categoria full name.
     *
     * @return string
     */
    public function getCategoriaStrAttribute() {

        if($this->categoria=="O"){

            return "Assembleia Ordinária";
        }elseif($this->categoria=="E"){

            return "Assembleia Extraordinária";
        }
        return "Não possui categoria";
    }
        

     /**
     * Get the data fim formatada.
     *
     * @return string
     */
    public function getDataFimStrAttribute() {

        if($this->data_fim){
            return DateTimeHelper::formatarDataCliente($this->data_fim);
        }
        return "";
    }

     /**
     * Get the data inicio formatada.
     *
     * @return string
     */
    public function getDataInicioStrAttribute() {

        if($this->data_inicio){
            return DateTimeHelper::formatarDataCliente($this->data_inicio);
        }
        return "";
    }

   

}