<?php

namespace App;

use App\Condomino;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'admin','condomino_id', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Relacionamento OneToOne - Um usuario pertence a um condomino 
    public function condomino(){
        
        return $this->belongsTo(Condomino::class);
    }

     /**
     * Get the categoria full name.
     *
     * @return string
     */
    public function getAdminStrAttribute() {

        if($this->admin=="1"){

            return "Administrador";
        }
        return "Usuário Comum";
    }
}
