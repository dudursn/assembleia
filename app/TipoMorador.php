<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoMorador extends Model
{
    //
      
    protected $table = 'tipo_moradores';

    protected $fillable = ["nome"];

    //Relacionamento OneToMany - Um tipo de morador tem varios condominos
    public function condominos(){

        return $this->hasMany(Condomino::class);
    }


}
