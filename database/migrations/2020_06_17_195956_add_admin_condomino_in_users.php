<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminCondominoInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            if(!Schema::getColumnListing('admin')){
                $table->tinyInteger('admin')->after('email')->default('0');
            }

            if(!Schema::getColumnListing('condomino_id')){
                //Chave estrangeira  e delete on cascade
                $table->unsignedBigInteger("condomino_id")->after('admin');
                $table->foreign("condomino_id")
                    ->references("id")
                    ->on("condominos");
                   
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            Schema::table('users', function($table) {
                $table->dropColumn('admin');
                $table->dropColumn('condomino_id');
            });
        });
    }
}
