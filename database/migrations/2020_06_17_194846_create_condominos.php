<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCondominos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condominos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome','200');
            $table->string('email')->unique();
            $table->string('cpf','11')->nullable();
            $table->string('cnpj','14')->nullable();
            $table->char('sexo','1')->nullable();
            $table->char('tel_fixo', '10')->nullable();
            $table->char('tel_celular', '11')->nullable();
           
            //Chave estrangeira  e delete without  cascade
            $table->unsignedBigInteger("tipo_pessoa_id");
            $table->foreign("tipo_pessoa_id")
                ->references("id")
                ->on("tipo_pessoas");

            //Chave estrangeira  e delete without  cascade
            $table->unsignedBigInteger("tipo_morador_id");
            $table->foreign("tipo_morador_id")
                ->references("id")
                ->on("tipo_moradores");

            //Chave estrangeira  e delete without  cascade
            $table->unsignedBigInteger("unidade_id");
            $table->foreign("unidade_id")
                ->references("id")
                ->on("unidades");

            $table->timestamps();
               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condominos');
    }
}
