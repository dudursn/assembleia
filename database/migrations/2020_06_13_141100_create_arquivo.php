<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArquivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arquivos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('nome','200');
            $table->char('extensao','6');
            $table->char('path','400');
            $table->timestamps();

            //Chave estrangeira  e delete on cascade
            $table->unsignedBigInteger("assembleia_id");
            $table->foreign("assembleia_id")
                ->references("id")
                ->on("assembleias");
               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arquivos');
    }
}
