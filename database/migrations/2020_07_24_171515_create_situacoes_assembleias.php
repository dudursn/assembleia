<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSituacoesAssembleias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('situacoes_assembleias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', '200');
            $table->timestamps();
        });

        Schema::table('assembleias', function (Blueprint $table) {
          

            //Chave estrangeira  e delete on cascade
            $table->unsignedBigInteger("situacao_assembleia_id")->after('hora_fim');
            $table->foreign("situacao_assembleia_id")
                ->references("id")
                ->on("situacoes_assembleias");
                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situacoes_assembleias');

        Schema::table('assembleias', function (Blueprint $table) {
            Schema::table('assembleias', function($table) {
              
                $table->dropColumn('situacao_assembleia_id');
            });
        });
    }
}
