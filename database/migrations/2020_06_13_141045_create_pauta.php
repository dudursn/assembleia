<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePauta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pautas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->char('assunto', '200');
            $table->string('descricao','400');
            $table->date('data_inicio');
            $table->date('data_fim');
            $table->char('hora_inicio', '5');
            $table->char('hora_fim', '5');

            $table->timestamps();

            //Chave estrangeira  e delete on cascade
            $table->unsignedBigInteger("assembleia_id");
            $table->foreign("assembleia_id")
                ->references("id")
                ->on("assembleias")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pautas');
    }
}
