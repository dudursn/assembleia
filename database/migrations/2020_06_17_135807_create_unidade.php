<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnidade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('numero');
            $table->string('nome_completo', '300');
            $table->decimal('fracao', 10, 9);
            $table->timestamps();

            //Chave estrangeira  e sem delete on cascade
            $table->unsignedBigInteger("torre_id");
            $table->foreign("torre_id")
                ->references("id")
                ->on("torres");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades');
    }
}
