<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssembleia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assembleias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('categoria', '1');
            $table->date('data_inicio');
            $table->date('data_fim');
            $table->char('hora_inicio', '5');
            $table->char('hora_fim', '5');
            $table->string('link');
            $table->string('observacoes')->nullable();
        
            $table->timestamps();

          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assembleia');
    }
}
