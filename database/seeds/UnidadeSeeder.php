<?php

use Illuminate\Database\Seeder;

class UnidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unidades = $this->dados();

        // check if table unidades is empty
        if(DB::table("unidades")->get()->count() == 0){

            DB::table("unidades")->insert($unidades);
        }
    }

    private function dados(){

        $arrayDados = [];

        for($i=1; $i<=15; $i++){
            $unidade = $i * 100;
            for($j=1; $j<=6; $j++){
                $numero = $unidade + $j;
                if($j == 2 || $j==6){
                    $fracao = 0.002554757;
                }else{
                    $fracao = 0.002372561;
                }

                $dado = [ 
                    "numero" => $numero,
                    "fracao" => $fracao,
                    "torre_id" => 1,
                    "created_at" => now(),
                    "updated_at" => now()

                ];

                $arrayDados[] = $dado; 
            
            }

        
        }
        for($k = 1; $k<=2; $k++){

            $torre_id = $k + 1;
            $nome_torre =$k+2;

            for($i=1; $i<=15; $i++){

                $unidade = $i * 100;
                for($j=1; $j<=12; $j++){
                    $numero = $unidade + $j;

                    if($numero ==1512){
                        $fracao = 0.002473799;
                    }else if(in_array($j, [1,6,7,12] )){
                        $fracao = 0.002471706;
                    }else if(in_array($j, [9,10] )){
                        $fracao = 0.002253458;
                    }else{
                        $fracao = 0.001939928;
                    }
                
                    $dado = [ 
                        "numero" => $numero,
                        "fracao" => $fracao,
                        "torre_id" => $torre_id,
                        "created_at" => now(),
                        "updated_at" => now()

                    ];

                    $arrayDados[] = $dado;  
                    
                }
            }
        
        }

        return $arrayDados;
    }
}
