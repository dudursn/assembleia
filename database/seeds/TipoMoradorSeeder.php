<?php

use Illuminate\Database\Seeder;

class TipoMoradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tipoMoradores = $this->dados();

        // check if table tipo_moradores is empty
        if(DB::table("tipo_moradores")->get()->count() == 0){

            DB::table("tipo_moradores")->insert($tipoMoradores);
        }
    }

    private function dados(){

        $arrayDados = [
            [
                "nome" => "Proprietário",
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "nome" => "Morador",
                "created_at" => now(),
                "updated_at" => now()
            ]
        ];

        return $arrayDados;

    }
}
