<?php

use Illuminate\Database\Seeder;

class TipoPessoaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tipoPessoas = $this->dados();

        // check if table tipo_pessoas is empty
        if(DB::table("tipo_pessoas")->get()->count() == 0){

            DB::table("tipo_pessoas")->insert($tipoPessoas);
        }
    }

    private function dados(){

        $arrayDados = [
            [
                "nome" => "Física",
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "nome" => "Jurídica",
                "created_at" => now(),
                "updated_at" => now()
            ]
        ];

        return $arrayDados;

    }
}
