<?php

use Illuminate\Database\Seeder;

class TorreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tipoTorres = $this->dados();

        // check if table torres is empty
        if(DB::table("torres")->get()->count() == 0){

            DB::table("torres")->insert($tipoTorres);
        }
    }

    private function dados(){

        
        $arrayDados = [
            [
                "nome" => "Torre 2",
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "nome" => "Torre 3",
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "nome" => "Torre 4",
                "created_at" => now(),
                "updated_at" => now()
            ]
        ];

        return $arrayDados;

    }


}
